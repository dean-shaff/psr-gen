pulsar_period = 0.00575745;
len_fft = 1024;
phase_bins = 128;
bw = 0.01;
sampling_rate = (1/bw)*1e-6;
times = (0:len_fft-1)*sampling_rate;

phases = findphase(times, phase_bins, struct('a', pulsar_period));

file_name = sprintf('./../test/findphase__%i__%i__%.2f__%.4f.dat', len_fft, phase_bins, bw, pulsar_period);
fid = fopen(file_name, 'w');

fwrite(fid, phases, 'double');

fclose(fid);
