len_fft = 2048;
nchan = 1;
% DM = 2.64476;
DM = 0.1;
bw = 5


sampling_period = (1/bw) * 1e-6;

sampling_period

freq_range = [1402.5, 1407.5];

[H, f, n_hi, n_lo] = dispnmatrix(freq_range, bw, len_fft, nchan, DM * 4.148804e3, sampling_period);

n_hi
n_lo

H_flat = reshape(H, 1, len_fft * nchan);
H_flat_double = zeros(1, 2*len_fft*nchan);

H_flat_double(1:2:end) = real(H_flat);
H_flat_double(2:2:end) = imag(H_flat);

file_name = sprintf('./../test/dispersion_matrix__%i-%i__%i__%i__%.4f.dat', freq_range(1), freq_range(2), len_fft, nchan, DM);
fid = fopen(file_name, 'w');

fwrite(fid, H_flat_double, 'double');

fclose(fid);
