len = 128;
noise = 0.0;

[S, J, p] = rotvecmod(len, noise);


file_name = sprintf('./../test/rotvecmod__%i__%.1f.dat', len, noise);
fid = fopen(file_name, 'w');

write_data_to_fid(fid, S);
write_data_to_fid(fid, J);
write_data_to_fid(fid, p);

fclose(fid);

function write_data_to_fid(fid, dat)

  dat_flat = dat(:);

  if isreal(dat_flat)
    fwrite(fid, dat_flat, 'double');
  else
    size_dat_flat = size(dat_flat);
    dat_flat2 = zeros(1, 2*size_dat_flat(1));
    dat_flat2(1:2:end) = real(dat_flat);
    dat_flat2(2:2:end) = imag(dat_flat);
    fwrite(fid, dat_flat2, 'double');
  end

end
