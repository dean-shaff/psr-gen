function write_simulated_pulsar ()
  % nblocks = 5;
  nblocks = 38;
  npol = 2;
  ndim = 2;
  centre_freq = 1405;
  % bw = 5;
  bw = 40;
  % noise = 0.8;
  noise = 0.0;
  pulsar_period = 0.00575745;
  DM = 0.1;
  % DM = 1.0;
  len_fft = 2048;
  % len_fft = 16384;
  len_fft = 1048576;
  % pulse_bins = 512;
  pulse_bins = 1024;
  % pulse_bins = 2048;
  precision = 'single';
  header_file_name = 'default_header.json';
  verbose = 0;
  test_mode = 0;

  tic
  file_name = simulated_pulsar_generator(...
    nblocks, npol, ndim, centre_freq, bw, noise, pulsar_period,...
    DM, len_fft, pulse_bins, precision, header_file_name, verbose, test_mode);
  toc
  movefile(file_name, './../test/test_data');

    % simulated_pulsar_generator(...
    %   1, 2, 2, 1405, 5, 0.8, 0.00575745, 0.1, 2048, 512, 'single', 'default_header.json', 1, 1)

    % actual pulsar period
    % simulated_pulsar_generator(...
    %   20, 2, 2, 1405, 40, 0.9, 0.00575745, 2.64476, 2^20, 2^10, 'single', 'default_header.json', 1)

    % super slow
    % simulated_pulsar_generator(...
    %   20, 2, 2, 1405, 40, 0.9, 0.5, 2.64476, 2^20, 2^10, 'single', 'default_header.json', 1)

    % super sped up
    % simulated_pulsar_generator(...
    %   20, 2, 2, 1405, 40, 0.9, 0.0001, 2.64476, 2^20, 2^10, 'single', 'default_header.json', 1)


end
