## 0.1.0

- Initial Release. Multi core, numerically correct pulsar generator.

## 0.2.0

- write_psr.cpp is now controllable via a simple command line interface.
- engineified pulsar generator (looking towards CUDA engine in long run).

## 0.3.0

- Add CUDA engine. Not fully optimized, but faster than 4 core OpenMP.
- Put benchmarks in src
- Get rid of examples, create dedicated write_psr executable.
- Using command line argument parser for executables.

## 0.3.1

- Add methods that allow for resizing data containers on our own, instead of allowing psr_gen::PulsarGenerator to do it. This means we could choose to not use `thrust::device_vector` if we wanted to.
- Fixed bug in .gitlab-ci.yml where it wouldn't update version badge
- Improved test coverage
- Moved `PulsarGeneratorEngineCUDA_wrapper.hpp` to public API area.

## 0.4.0

- Add Python support
