#ifndef __cuda_wrapper_hpp
#define __cuda_wrapper_hpp

#include <vector>
#include <complex>

namespace test {
namespace util {
namespace cuda {

template<typename Type>
void cholesky_2d(
  std::vector<Type>& out,
  const std::string& side,
  bool conj
);

}
}
}

#endif
