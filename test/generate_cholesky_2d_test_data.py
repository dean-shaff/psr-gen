import sys

import numpy as np
import scipy.linalg


def main(nmatrices: int):

    mat_real = np.zeros((nmatrices*2, 4), dtype=np.float64)
    mat_comp = np.zeros((nmatrices*2, 4), dtype=np.complex128)

    for idx in range(nmatrices):
        a_r = np.random.rand(2, 2)
        a_r = np.dot(a_r.T, a_r)
        chol_r = scipy.linalg.cholesky(a_r)

        mat_real[2*idx, :] = a_r.flatten()
        mat_real[2*idx + 1, :] = chol_r.flatten()

        a_c = np.random.rand(2, 2) + 1j*np.random.rand(2, 2)
        a_c = np.dot(a_c.conj().T, a_c)
        chol_c = scipy.linalg.cholesky(a_c)

        mat_comp[2*idx, :] = a_c.flatten()
        mat_comp[2*idx + 1, :] = chol_c.flatten()

    file_names = [
        f"test_data/cholesky_2d.{nmatrices}.real.dat",
        f"test_data/cholesky_2d.{nmatrices}.comp.dat",
    ]

    arrays = [
        mat_real,
        mat_comp
    ]

    for file_name, array in zip(file_names, arrays):
        with open(file_name, "wb") as fd:
            fd.write(array.tobytes())


if __name__ == "__main__":
    main(int(sys.argv[1]))
