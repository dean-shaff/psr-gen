
#include <iostream>
#include <complex>
#include <vector>

#include "catch.hpp"
#include "psr_gen/util/cholesky_2d.hpp"

#include "cuda_wrapper.hpp"
#include "util.hpp"


static test::util::TOMLConfig config;


template<typename Type>
struct _type_map {
  typedef double type;
};

template<typename Type>
struct _type_map<std::complex<Type>> {
  typedef std::complex<double> type;
};

template<typename Type>
void get_test_matrices (
  const std::string& toml_path_base,
  std::vector<
    std::vector<
      std::vector<Type>>>& matrices
)
{

  typedef typename _type_map<Type>::type load_type;

  std::string test_dir = test::util::get_env_var("TEST_DIR", "./../test");

  std::string file_name = config.get<std::string>(
    toml_path_base + ".name"
  );
  std::string file_path = test_dir + "/" + file_name;

  unsigned nmatrices = (unsigned) config.get<int>(
    toml_path_base + ".nmatrices"
  );

  matrices.resize(nmatrices);

  std::vector<load_type> data;
  test::util::load_from_binary(file_path, data);

  unsigned offset = 0;
  for (unsigned idx=0; idx<nmatrices; idx++) {
    matrices[idx].resize(2);
    matrices[idx][0] = std::vector<Type>(data.begin() + offset, data.begin() + offset + 4);
    matrices[idx][1] = std::vector<Type>(data.begin() + offset + 4, data.begin() + offset + 8);
    offset += 8;
  }

}


TEMPLATE_TEST_CASE(
  "C++ 2D cholesky decomposition produces correct result",
  "[util][unit]",
  float, double
)
{
  config.load_config();

  double atol = config.get<double>("test_util.atol");
  double rtol = config.get<double>("test_util.rtol");

  std::vector<std::vector<std::vector<TestType>>> real_matrices;
  std::vector<std::vector<std::vector<std::complex<TestType>>>> comp_matrices;

  get_test_matrices<TestType>(
    "test_util.test_cholesky_2d_real_file", real_matrices);

  get_test_matrices<std::complex<TestType>>(
    "test_util.test_cholesky_2d_complex_file", comp_matrices);

  SECTION("Correct for real data types")
  {
    std::vector<TestType> test_upper;
    std::vector<TestType> test_lower;
    std::vector<TestType> expected;
    for (unsigned idx=0; idx<real_matrices.size(); idx++) {

      test_upper = std::vector<TestType>(
        real_matrices[idx][0].begin(),
        real_matrices[idx][0].end());

      test_lower = std::vector<TestType>(
        real_matrices[idx][0].begin(),
        real_matrices[idx][0].end());

      expected = real_matrices[idx][1];
      psr_gen::util::cholesky_2d(test_upper.data(), "u", true);
      psr_gen::util::cholesky_2d(test_lower.data(), "l", true);

      unsigned nclose = test::util::nclose(test_upper, expected, atol, rtol);
      CHECK(nclose == 4);

      expected[2] = expected[1];
      expected[1] = 0.0;
      nclose = test::util::nclose(test_lower, expected, atol, rtol);
      CHECK(nclose == 4);

    }
  }

  SECTION("Correct for complex data types")
  {
    std::vector<std::complex<TestType>> test;
    std::vector<std::complex<TestType>> expected;
    for (unsigned idx=0; idx<comp_matrices.size(); idx++) {

      test = std::vector<std::complex<TestType>>(
        comp_matrices[idx][0].begin(), comp_matrices[idx][0].end());

      expected = comp_matrices[idx][1];
      psr_gen::util::cholesky_2d(test.data(), "u", true);

      unsigned nclose = test::util::nclose(test, expected, atol, rtol);
      CHECK(nclose == 4);

    }
  }

}


TEMPLATE_TEST_CASE(
  "CUDA 2D cholesky decomposition produces correct result",
  "[util][unit][cuda]",
  float//, double
)
{
  config.load_config();

  double atol = config.get<double>("test_util.atol");
  double rtol = config.get<double>("test_util.rtol");

  std::vector<std::vector<std::vector<TestType>>> real_matrices;
  std::vector<std::vector<std::vector<std::complex<TestType>>>> comp_matrices;

  get_test_matrices<TestType>(
    "test_util.test_cholesky_2d_real_file", real_matrices);

  get_test_matrices<std::complex<TestType>>(
    "test_util.test_cholesky_2d_complex_file", comp_matrices);

  SECTION("Correct for real data types")
  {
    std::vector<TestType> test;
    std::vector<TestType> expected;
    for (unsigned idx=0; idx<real_matrices.size(); idx++) {

      test = std::vector<TestType>(real_matrices[idx][0].begin(), real_matrices[idx][0].end());
      expected = real_matrices[idx][1];
      test::util::cuda::cholesky_2d(test, "u", true);

      unsigned nclose = test::util::nclose(test, expected, atol, rtol);
      CHECK(nclose == 4);

    }
  }

  SECTION("Correct for complex data types")
  {
    std::vector<std::complex<TestType>> test;
    std::vector<std::complex<TestType>> expected;
    for (unsigned idx=0; idx<comp_matrices.size(); idx++) {

      test = std::vector<std::complex<TestType>>(
        comp_matrices[idx][0].begin(), comp_matrices[idx][0].end());

      expected = comp_matrices[idx][1];
      test::util::cuda::cholesky_2d(test, "u", true);

      unsigned nclose = test::util::nclose(test, expected, atol, rtol);
      CHECK(nclose == 4);

    }
  }

}
