#define CATCH_CONFIG_RUNNER

#include "catch.hpp"

#include "psr_gen/config.hpp"


int main( int argc, char* argv[] )
{

  psr_gen::config::verbose() = false;

  for (int i=0; i<argc; i++) {
    if (strcmp(argv[i], "-v") == 0) {
      psr_gen::config::verbose() = true;
    }
  }

  Catch::Session session; // There must be exactly one instance

  int returnCode = session.applyCommandLine( argc, argv );
  if ( returnCode != 0 ) { // Indicates a command line error
    return returnCode;
  }

  int numFailed = session.run();
  return numFailed;
}
