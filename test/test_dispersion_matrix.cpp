#include "tuple"

#include "catch.hpp"

#include "psr_gen/util/dispersion_matrix.hpp"
#include "util.hpp"

static test::util::TOMLConfig config;


TEMPLATE_TEST_CASE(
  "DispersionMatrix behaves as expected",
  "[dispersion_matrix][unit]",
  float, double
)
{
  SECTION ("can construct matrix object")
  {
    psr_gen::util::DispersionMatrix<TestType> matrix (
      std::make_tuple(0, 1000), 2.64476
    );
  }

  SECTION ("getters and setters work")
  {
    psr_gen::util::DispersionMatrix<TestType> matrix (
      std::make_tuple(0, 1000), 2.64476
    );
    auto freq_range = matrix.get_freq_range();

    CHECK(std::get<0>(freq_range) == 0);
    CHECK(std::get<1>(freq_range) == 1000);

    double dm = matrix.get_DM();

    CHECK(dm == 2.64476);

    double usable_bw = matrix.get_usable_bandwidth();

    CHECK(usable_bw == 1.0);

  }

}


TEST_CASE(
  "generate correct dispersion matrix",
  "[dispersion_matrix][integration]"
)
{
  std::string test_dir = test::util::get_env_var("TEST_DIR", "./../test");

  struct file_info {
    std::string file_name;
    std::vector<double> freq_range;
    double DM;
    unsigned len_fft;
    unsigned nchan;
    double sampling_period;
    unsigned n_disp_low;
    unsigned n_disp_high;
  };

  config.load_config();

  const toml::Array toml_file_config = config.get<toml::Array>(
    "test_dispersion_matrix.file");

  std::vector<file_info> file_config (toml_file_config.size());

  for (unsigned idx=0; idx<file_config.size(); idx++) {
    file_config[idx].file_name = toml_file_config[idx].get<std::string>("name");
    file_config[idx].freq_range = toml_file_config[idx].get<std::vector<double>>("freq_range");
    file_config[idx].DM = toml_file_config[idx].get<double>("DM");
    file_config[idx].len_fft = (unsigned) toml_file_config[idx].get<int>("len_fft");
    file_config[idx].nchan = (unsigned) toml_file_config[idx].get<int>("nchan");
    file_config[idx].sampling_period = toml_file_config[idx].get<double>("sampling_period");
    file_config[idx].n_disp_low = toml_file_config[idx].get<int>("n_disp_low");
    file_config[idx].n_disp_high = toml_file_config[idx].get<int>("n_disp_high");
  }

  auto test_idx = GENERATE_COPY(range(0, (int) file_config.size()));

  file_info info = file_config[test_idx];

  psr_gen::util::DispersionMatrix<double> matrix (
    std::make_tuple(info.freq_range[0], info.freq_range[1]), info.DM
  );

  std::vector<std::complex<double>> result;
  int n_disp_low;
  int n_disp_high;
  int direction = -1;

  matrix.compute(
    result,
    n_disp_low,
    n_disp_high,
    info.len_fft,
    info.nchan,
    info.sampling_period,
    direction
  );

  CHECK(n_disp_low == info.n_disp_low);
  CHECK(n_disp_high == info.n_disp_high);

  std::vector<std::complex<double>> expected;

  test::util::load_from_binary(test_dir + "/" + info.file_name, expected);

  CHECK(result.size() == expected.size());

  unsigned nclose = test::util::nclose(expected, result);

  std::cerr << nclose << "/" << expected.size()
    << " " << (float) nclose * 100 / expected.size() << " % equal" << std::endl;

  REQUIRE(nclose == expected.size());

}
