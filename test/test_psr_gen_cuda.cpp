#include <tuple>

#include <cuda_runtime.h>
#include "catch.hpp"

#include "psr_gen/config.hpp"
#include "psr_gen/PulsarGeneratorEngineCUDA_wrapper.hpp"
#include "util.hpp"

static test::util::TOMLConfig config;


TEMPLATE_TEST_CASE(
  "can call psr_gen::PulsarGenerator::operate with PulsarGeneratorEngineCUDA engine",
  "[psr_gen][cuda][unit]",
  float, double
)
{
  psr_gen::config::verbose() = false;
  psr_gen::PulsarGenerator<
    TestType, psr_gen::PulsarGeneratorEngineCUDA<TestType>
  > gen(5.0, 1405, 2, 0.1, 0.00575745, 0.8, 2048, 512);

  SECTION ("Let PulsarGenerator resize output array")
  {
    std::vector<std::complex<TestType>> out;
    psr_gen::PulsarGenerator_operate<TestType>(gen, 0.005, out);
  }

  SECTION ("Allocate memory ourselves")
  {
    std::complex<TestType>* out;
    unsigned nsamples = gen.samples_for(0.005);
    cudaMalloc((void**) &out, nsamples*sizeof(std::complex<TestType>));
    psr_gen::PulsarGenerator_operate<TestType>(gen, 0.005, out);
    cudaFree(out);
  }

}

TEMPLATE_TEST_CASE(
  "psr_gen::PulsarGeneratorEngineOpenMP and psr_gen::PulsarGeneratorEngineCUDA produce same result",
  "[psr_gen][cuda][integration]",
  float
)
{
  psr_gen::config::verbose() = true;
  config.load_config();

  double atol = config.get<double>("test_psr_gen.atol");
  double rtol = config.get<double>("test_psr_gen.rtol");
  double DM = config.get<double>("test_psr_gen.DM");
  double bandwidth = config.get<double>("test_psr_gen.bandwidth");
  double centre_freq = config.get<double>("test_psr_gen.centre_freq");
  unsigned npol = (unsigned) config.get<int>("test_psr_gen.npol");
  double period = config.get<double>("test_psr_gen.period");
  double noise = config.get<double>("test_psr_gen.noise");
  unsigned len_fft = (unsigned) config.get<int>("test_psr_gen.len_fft");
  unsigned phase_bins = (unsigned) config.get<int>("test_psr_gen.phase_bins");
  double time = config.get<double>("test_psr_gen.time");

  psr_gen::PulsarGenerator<
    TestType, psr_gen::PulsarGeneratorEngineCUDA<TestType>
  > gen_cuda(bandwidth, centre_freq, npol, DM, period, noise, len_fft, phase_bins);
  std::vector<std::complex<TestType>> out_cuda;
  psr_gen::PulsarGenerator_operate<TestType>(gen_cuda, time, out_cuda);

  psr_gen::PulsarGenerator<TestType> gen(
    bandwidth, centre_freq, npol, DM, period, noise, len_fft, phase_bins);
  std::vector<std::complex<TestType>> out;
  gen.compute(time, out);

  unsigned nclose = test::util::nclose(out, out_cuda, atol, rtol);

  std::cerr << "psr_gen cuda: " <<  nclose << "/" << out.size()
    << " " << (double) nclose * 100 / out.size() << " % equal" << std::endl;

  REQUIRE(nclose == out.size());
}
