#include <thrust/device_vector.h>

#include "psr_gen/util/cuda/PulsarGeneratorEngineCUDA_kernels.cuh"

#include "cuda_wrapper.hpp"

template<typename Type>
struct std2cufft;

template<>
struct std2cufft<float>
{
  typedef float type;
};

template<>
struct std2cufft<double>
{
  typedef double type;
};

template<>
struct std2cufft<std::complex<float>>
{
  typedef cufftComplex type;
};

template<>
struct std2cufft<std::complex<double>>
{
  typedef cufftDoubleComplex type;
};

// template<typename Type>
// void test::util::cuda::PulsarGenerator_operate(
//   test::util::cuda::PulsarGeneratorCUDA<Type>& gen,
//   double time,
//   std::vector<std::complex<Type>>& out
// )
// {
//   thrust::device_vector<std::complex<Type>> out_device;
//   gen.compute(time, out_device);
//   out.resize(out_device.size());
//   thrust::copy(
//     out_device.begin(),
//     out_device.end(),
//     out.begin());
// }
//
// template void test::util::cuda::PulsarGenerator_operate<float>(
//   test::util::cuda::PulsarGeneratorCUDA<float>& gen,
//   double time,
//   std::vector<std::complex<float>>& out
// );
//
// template void test::util::cuda::PulsarGenerator_operate<double>(
//   test::util::cuda::PulsarGeneratorCUDA<double>& gen,
//   double time,
//   std::vector<std::complex<double>>& out
// );

template<typename Type>
__global__ void _cholesky_2d (
  Type* arr,
  const char side,
  bool conj
)
{
  if (threadIdx.x == 0) {
    psr_gen::util::cuda::cholesky_2d(arr, side, conj);
  }
}

template __global__ void _cholesky_2d<float>(float* arr, const char side, bool conj);
template __global__ void _cholesky_2d<double>(double* arr, const char side, bool conj);
template __global__ void _cholesky_2d<cufftComplex>(cufftComplex* arr, const char side, bool conj);
template __global__ void _cholesky_2d<cufftDoubleComplex>(cufftDoubleComplex* arr, const char side, bool conj);


template<typename Type>
void test::util::cuda::cholesky_2d(
  std::vector<Type>& out,
  const std::string& side,
  bool conj
)
{

  typedef typename std2cufft<Type>::type cufft_type;

  thrust::device_vector<Type> out_device(
    out.begin(), out.end());

  _cholesky_2d<cufft_type><<<1, 1>>>(
    (cufft_type*) thrust::raw_pointer_cast(&out_device[0]),
    side[0], conj);

  thrust::copy(
    out_device.begin(),
    out_device.end(),
    out.begin());
}

template void test::util::cuda::cholesky_2d<float>(std::vector<float>& out, const std::string& side, bool conj);
template void test::util::cuda::cholesky_2d<double>(std::vector<double>& out, const std::string& side, bool conj);
template void test::util::cuda::cholesky_2d<std::complex<float>>(std::vector<std::complex<float>>& out, const std::string& side, bool conj);
template void test::util::cuda::cholesky_2d<std::complex<double>>(std::vector<std::complex<double>>& out, const std::string& side, bool conj);
