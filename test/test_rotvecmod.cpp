#include <vector>
#include <complex>
#include <algorithm>
#include <iostream>
#include <tuple>

#include "catch.hpp"

#include "psr_gen/util/rotvecmod.hpp"
#include "util.hpp"

static test::util::TOMLConfig config;

TEST_CASE(
  "can call rotvecmod",
  "[rotvecmod][unit]"
)
{
  unsigned size = 1024;
  double noise = 0.2;

  SECTION("will resize arrays if necessary")
  {
    std::vector<std::vector<double>> stokes;
    std::vector<std::vector<std::complex<double>>> coherency;
    std::vector<double> phases;

    psr_gen::util::rotvecmod(size, noise, stokes, coherency, phases);

    CHECK(stokes.size() == 4);
    CHECK(coherency.size() == 4);
    CHECK(phases.size() == size);
    CHECK(stokes[0].size() == size);
    CHECK(coherency[0].size() == size);
  }

  SECTION("works with already allocated output arrays")
  {
    std::vector<std::vector<double>> stokes(4);
    std::vector<std::vector<std::complex<double>>> coherency(4);
    std::vector<double> phases;

    psr_gen::util::rotvecmod(size, noise, stokes, coherency, phases);
    CHECK(phases.size() == size);
    CHECK(stokes[0].size() == size);
    CHECK(coherency[0].size() == size);
  }



}


TEST_CASE(
  "rotvecmod produces correct result",
  "[rotvecmod][integration]"
)
{
  std::string test_dir = test::util::get_env_var("TEST_DIR", "./../test");

  typedef std::tuple<std::string, double, unsigned> file_info;

  config.load_config();
  const toml::Array toml_file_config = config.get<toml::Array>(
    "test_rotvecmod.file");

  std::vector<file_info> file_config (toml_file_config.size());

  for (unsigned idx=0; idx<file_config.size(); idx++) {
    std::get<0>(file_config[idx]) = toml_file_config[idx].get<std::string>("name");
    std::get<1>(file_config[idx]) = toml_file_config[idx].get<double>("noise");
    std::get<2>(file_config[idx]) = (unsigned) toml_file_config[idx].get<int>("size");
  }

  auto test_idx = GENERATE_COPY(range(0, (int) file_config.size()));

  file_info info = file_config[test_idx];

  std::string file_name = std::get<0>(info);
  double noise = std::get<1>(info);
  unsigned size = std::get<2>(info);

  // std::cerr << "file_name=" << file_name << std::endl;
  // std::cerr << "size=" << size << std::endl;
  // std::cerr << "noise=" << noise << std::endl;

  std::vector<std::vector<double>> stokes(4);
  std::vector<std::vector<std::complex<double>>> coherency(4);
  std::vector<double> phases;

  std::vector<std::vector<double>> stokes_expected(4);
  std::vector<std::vector<std::complex<double>>> coherency_expected(4);
  std::vector<double> phases_expected;

  psr_gen::util::rotvecmod(size, noise, stokes, coherency, phases);

  std::vector<double> expected;

  test::util::load_from_binary(test_dir + "/" + file_name, expected);

  unsigned offset = 0;
  unsigned nclose = 0;

  for (unsigned idx=0; idx<stokes_expected.size(); idx++) {
    stokes_expected[idx].resize(size);
    std::copy(
      expected.data() + offset,
      expected.data() + offset + size,
      stokes_expected[idx].data());
    nclose = test::util::nclose(stokes_expected[idx], stokes[idx]);
    std::cerr << "stokes[" << idx << "]: " <<  nclose << "/" << size
      << " " << (float) nclose * 100 / size << " % equal" << std::endl;
    CHECK(nclose == size);

    offset += size;
  }
  for (unsigned idx=0; idx<coherency_expected.size(); idx++) {
    coherency_expected[idx].resize(size);
    std::copy(
      reinterpret_cast<std::complex<double>*>(expected.data() + offset),
      reinterpret_cast<std::complex<double>*>(expected.data() + offset + 2*size),
      coherency_expected[idx].data());
    nclose = test::util::nclose(coherency_expected[idx], coherency[idx]);
    std::cerr << "coherency[" << idx << "]: " <<  nclose << "/" << size
      << " " << (float) nclose * 100 / size << " % equal" << std::endl;
    CHECK(nclose == size);

    offset += 2*size;
  }

  phases_expected.resize(size);

  std::copy(
    expected.data() + offset,
    expected.data() + offset + size,
    phases_expected.data());

  nclose = test::util::nclose(phases_expected, phases);
  std::cerr << "phases: " <<  nclose << "/" << size
    << " " << (float) nclose * 100 / size << " % equal" << std::endl;
  CHECK(nclose == size);
}
