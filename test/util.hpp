#ifndef __test_util_hpp
#define __test_util_hpp

#include <cstdlib>
#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <complex>

#include "toml.h"

namespace test {
namespace util {

inline std::string get_env_var (
  const std::string& env_var_name,
  const std::string& default_val
)
{
  const char* env_var = std::getenv(env_var_name.c_str());
  if (env_var) {
    return std::string(env_var);
  } else {
    return default_val;
  }
}


template<typename T>
bool isclose (T a, T b, float atol=1e-7, float rtol=1e-5)
{
  return std::abs(a - b) <= (atol + rtol*std::abs(b));
}

template<typename T>
bool isclose (
  std::complex<T> a, std::complex<T> b,
  float atol=1e-7, float rtol=1e-5)
{
  return isclose(a.real(), b.real(), atol, rtol) &&
         isclose(a.imag(), b.imag(), atol, rtol);
}

template<typename T>
T reldiff (
  T a, T b
)
{
  return std::abs(a - b)/std::abs(b);
}

template<typename T>
std::complex<T> reldiff(
  std::complex<T> a, std::complex<T> b
)
{
  std::complex<T> ret;
  ret.real(reldiff<T>(a.real(), b.real()));
  ret.imag(reldiff<T>(a.imag(), b.imag()));

  return ret;
}


template<typename T, class ... Types>
unsigned nclose(
  const std::vector<T>& a,
  const std::vector<T>& b,
  Types ... args
)
{
  unsigned nclose_val = 0;
  if (a.size() != b.size()) {
    return nclose_val;
  }

  for (unsigned idx=0; idx < a.size(); idx++) {
    if (isclose(a[idx], b[idx], args...)) {
      nclose_val++;
    } else {
      std::cerr << idx << ": "<< a[idx] << ", " << b[idx] << " : " << reldiff(a[idx], b[idx]) << std::endl;
    }
  }
  return nclose_val;
}



template<typename T>
void load_from_binary (
  const std::string& file_path,
  std::vector<T>& result,
  unsigned skip=0
)
{
  std::streampos size;

  std::ifstream file (file_path, std::ios::in|std::ios::binary|std::ios::ate);
  if (file.is_open())
  {
    file.seekg(0, std::ios::end);
    size = file.tellg();
    file.seekg(0, std::ios::beg);

    // read the data:
    std::vector<char> file_bytes(size);
    file.read(&file_bytes[0], size);
    file.close();

    unsigned T_size = ((size) / sizeof(T));
    T_size -= skip / sizeof(T);

    const T* data = reinterpret_cast<const T*>(file_bytes.data() + skip);
    result.assign(data, data + T_size);
  }
}

class TOMLConfig {
public:

  TOMLConfig () {
    default_file_path = test::util::get_env_var(
      "TEST_DIR", "./../test") + "/" + "test.config.toml";
  }

  void load_config (const std::string& _file_path = "") {
    std::string file_path = _file_path;
    if (file_path == "") {
      file_path = default_file_path;
    }

    std::ifstream ifs(file_path);
    if (! ifs.good()) {
      throw "test::util::TOMLConfig::load_config: file_path is either nonexistent or locked";
    }
    toml::ParseResult pr = toml::parse(ifs);
    ifs.close();

    if (! pr.valid())
    {
      throw "test::util::TOMLConfig::load_config: invalid TOML file";
    }
    config = pr.value;
  }

  template<class T>
  T get (const std::string& name)
  {
    if (! config_loaded) {
      load_config();
    }

    return (T) config.get<T>(name);
  }



private:
  toml::Value config;
  bool config_loaded;

  std::string default_file_path;

};

}
}

#endif
