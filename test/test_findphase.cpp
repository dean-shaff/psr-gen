#include <vector>
#include <complex>
#include <algorithm>
#include <iostream>
#include <tuple>

#include "catch.hpp"

#include "psr_gen/util/findphase.hpp"
#include "util.hpp"


static test::util::TOMLConfig config;

TEST_CASE(
  "findphase produces correct result",
  "[findphase][integration]"
)
{
  std::string test_dir = test::util::get_env_var("TEST_DIR", "./../test");

  struct file_info {
    std::string name;
    unsigned len_fft;
    unsigned phase_bins;
    double bw;
    double period;
  };

  config.load_config();
  const toml::Array toml_file_config = config.get<toml::Array>(
    "test_findphase.file");

  std::vector<file_info> file_config (toml_file_config.size());

  for (unsigned idx=0; idx<file_config.size(); idx++) {
    file_config[idx].name = toml_file_config[idx].get<std::string>("name");
    file_config[idx].len_fft = (unsigned) toml_file_config[idx].get<int>("len_fft");
    file_config[idx].phase_bins = (unsigned) toml_file_config[idx].get<int>("phase_bins");
    file_config[idx].bw = toml_file_config[idx].get<double>("bw");
    file_config[idx].period = toml_file_config[idx].get<double>("period");
  }

  auto test_idx = GENERATE_COPY(range(0, (int) file_config.size()));

  file_info info = file_config[test_idx];

  std::string file_name = info.name;
  unsigned len_fft = info.len_fft;
  unsigned phase_bins = info.phase_bins;
  double bw = info.bw;
  double period = info.period;
  double sampling_rate = (1.0/bw)*1e-6;

  std::vector<double> times(len_fft);
  for (unsigned idx=0; idx<len_fft; idx++) {
    times[idx] = sampling_rate*idx;
  }

  std::vector<double> phases;
  std::vector<double> phases_expected(len_fft);


  psr_gen::util::findphase(times, phase_bins, period, phases);
  CHECK(phases.size() == len_fft);
  for (unsigned idx=0; idx<len_fft; idx++) {
    phases[idx]++;
  }

  // for (unsigned idx=0; idx<len_fft; idx++) {
  //   std::cerr << phases[idx] << " ";
  // }
  // std::cerr << std::endl;

  test::util::load_from_binary(test_dir + "/" + file_name, phases_expected);

  unsigned nclose = test::util::nclose(phases_expected, phases);
  std::cerr << "phases: " <<  nclose << "/" << len_fft
    << " " << (float) nclose * 100 / len_fft << " % equal" << std::endl;
  CHECK(nclose == len_fft);
}
