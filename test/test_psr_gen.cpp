#include "tuple"

#include "catch.hpp"

#include "psr_gen/config.hpp"
#include "psr_gen/PulsarGenerator.hpp"
#include "psr_gen/PulsarGeneratorEngineOpenMP.hpp"
#include "util.hpp"

static test::util::TOMLConfig config;


TEMPLATE_TEST_CASE(
  "can call psr_gen::PulsarGenerator with default engine",
  "[psr_gen][unit]",
  float, double
)
{
  psr_gen::PulsarGenerator<TestType> gen(40, 1405, 2, 0.1, 0.005, 0.8, 2048, 128);

  SECTION ("Throw runtime_error if DM is too large for len_fft")
  {
    REQUIRE_THROWS_AS(
      psr_gen::PulsarGenerator<TestType>(40, 1405, 2, 100, 0.005, 0.8, 2048, 128),
      std::runtime_error);
  }


  SECTION ("Allow PulsarGenerator to resize")
  {
    std::vector<std::complex<TestType>> out;
    gen.compute(0.005, out);
    REQUIRE(out.size() == gen.samples_for(0.005));
  }

  SECTION ("resize array ourselves, passing pointer")
  {
    std::vector<std::complex<TestType>> out;
    unsigned samples = gen.samples_for(0.005);
    out.resize(samples);
    gen.compute(0.005, out.data());
  }

  SECTION ("raise runtime error if time is too short for len_fft")
  {
    std::vector<std::complex<TestType>> out;
    REQUIRE_THROWS_AS(gen.compute(0.00001, out), std::runtime_error);
    REQUIRE_THROWS_AS(gen.compute(0.00001, out.data()), std::runtime_error);
  }

  SECTION ("compute number of processing blocks")
  {
    unsigned nblocks = gen.blocks_for(0.005);
    REQUIRE(nblocks == 127);
  }

}


TEST_CASE(
  "psr_gen::PulsarGenerator produces correct result",
  "[psr_gen][integration]"
)
{
  std::string test_dir = test::util::get_env_var("TEST_DIR", "./../test");
  config.load_config();

  struct file_info {
    double bandwidth;
    double centre_freq;
    unsigned npol;
    double DM;
    double period;
    double noise;
    unsigned len_fft;
    unsigned phase_bins;
    double time;
    unsigned header_size;
    std::string name;
  };

  config.load_config();
  const toml::Array toml_file_config = config.get<toml::Array>(
    "test_psr_gen.file");

  std::vector<file_info> file_config (toml_file_config.size());

  for (unsigned idx=0; idx<file_config.size(); idx++) {
    file_config[idx].bandwidth = toml_file_config[idx].get<double>("bandwidth");
    file_config[idx].centre_freq = toml_file_config[idx].get<double>("centre_freq");
    file_config[idx].npol = (unsigned) toml_file_config[idx].get<int>("npol");
    file_config[idx].DM = toml_file_config[idx].get<double>("DM");
    file_config[idx].period = toml_file_config[idx].get<double>("period");
    file_config[idx].noise = toml_file_config[idx].get<double>("noise");
    file_config[idx].len_fft = (unsigned) toml_file_config[idx].get<int>("len_fft");
    file_config[idx].phase_bins = (unsigned) toml_file_config[idx].get<int>("phase_bins");
    file_config[idx].time = toml_file_config[idx].get<double>("time");
    file_config[idx].header_size = (unsigned) toml_file_config[idx].get<int>("header_size");
    file_config[idx].name = toml_file_config[idx].get<std::string>("name");
  }

  auto test_idx = GENERATE_COPY(range(0, (int) file_config.size()));

  file_info info = file_config[test_idx];

  psr_gen::PulsarGenerator<double> gen(
    info.bandwidth,
    info.centre_freq,
    info.npol,
    info.DM,
    info.period,
    info.noise,
    info.len_fft,
    info.phase_bins);

  std::vector<std::complex<double>> out;
  std::vector<std::complex<double>> out_expected;

  test::util::load_from_binary(test_dir + "/" + info.name, out_expected, info.header_size);

  gen.compute(info.time, out);

  CHECK(out.size() == out_expected.size());

  // for (unsigned idx=0; idx<10; idx++) {
  //   std::cerr << out[idx] << ", " << out_expected[idx] << std::endl;
  // }


  unsigned nclose = test::util::nclose(out_expected, out, 1e-4, 1e-4);
  std::cerr << "psr_gen: " <<  nclose << "/" << out.size()
    << " " << (double) nclose * 100 / out.size() << " % equal" << std::endl;
  CHECK(nclose == out.size());

}
