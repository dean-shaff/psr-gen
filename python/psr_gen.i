%module psr_gen

%{
#define SWIG_FILE_WITH_INIT
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include "psr_gen/util/util.hpp"
#include "psr_gen/PulsarGenerator.hpp"
#include "psr_gen/PulsarGeneratorEngineOpenMP.hpp"
#include "psr_gen/PulsarGeneratorEngineCUDA.hpp"

#include "python_util.hpp"
%}

%init %{
  import_array();
%}

%typemap(out) psr_gen::util::two_tuple {
    PyTupleObject *res = (PyTupleObject *)PyTuple_New(2);
    PyTuple_SetItem((PyObject *)res, 0, PyFloat_FromDouble(std::get<0>($1)));
    PyTuple_SetItem((PyObject *)res, 1, PyFloat_FromDouble(std::get<1>($1)));
    $result = (PyObject *)res;
}

%include "psr_gen/util/util.hpp"
%include "psr_gen/PulsarGenerator.hpp"

%template(PulsarGeneratorFloatOpenMP) psr_gen::PulsarGenerator<float>;
%template(PulsarGeneratorDoubleOpenMP) psr_gen::PulsarGenerator<double>;
%template(PulsarGeneratorFloatCUDA) psr_gen::PulsarGenerator<float, psr_gen::PulsarGeneratorEngineCUDA<float>>;
%template(PulsarGeneratorDoubleCUDA) psr_gen::PulsarGenerator<double, psr_gen::PulsarGeneratorEngineCUDA<double>>;



// %init %{
// import_array();
// %}
//
//
// %include "pfb/PolyphaseFilterBankPlan.hpp"
// %include "pfb/util/util.hpp"
//
//
// %template(PFBPlanCPU_float) pfb::PolyphaseFilterBankPlan<std::complex<float>>;
// %template(PFBPlanCPU_double) pfb::PolyphaseFilterBankPlan<std::complex<double>>;
// %template(PFBPlanCUDA_float) pfb::PolyphaseFilterBankPlan<
//   cufftComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftComplex>>;
// %template(PFBPlanCUDA_double) pfb::PolyphaseFilterBankPlan<
//   cufftDoubleComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftDoubleComplex>>;
//
//
// %extend pfb::PolyphaseFilterBankPlan<std::complex<float>> {
//
//   PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
//   {
//
//     PyArrayObject* ret_arr = pfb::util::numpy_get_arr_from_obj(ret);
//     PyArrayObject* in_arr = pfb::util::numpy_get_arr_from_obj(in);
//     PyArrayObject* filter_arr = pfb::util::numpy_get_arr_from_obj(filter);
//
//     $self->operator() (
//       (std::complex<float>*) PyArray_BYTES(in_arr),
//       (std::complex<float>*) PyArray_BYTES(filter_arr),
//       (std::complex<float>*) PyArray_BYTES(ret_arr)
//     );
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
//
// %extend pfb::PolyphaseFilterBankPlan<std::complex<double>> {
//
//   PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
//   {
//
//     PyArrayObject* ret_arr = pfb::util::numpy_get_arr_from_obj(ret);
//     PyArrayObject* in_arr = pfb::util::numpy_get_arr_from_obj(in);
//     PyArrayObject* filter_arr = pfb::util::numpy_get_arr_from_obj(filter);
//
//     $self->operator() (
//       (std::complex<double>*) PyArray_BYTES(in_arr),
//       (std::complex<double>*) PyArray_BYTES(filter_arr),
//       (std::complex<double>*) PyArray_BYTES(ret_arr)
//     );
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
//
// %extend pfb::PolyphaseFilterBankPlan<
//   cufftComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftComplex>> {
//
//   PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
//   {
//
//     long long in_ptr = pfb::util::pycuda_get_ptr(in);
//     long long filter_ptr = pfb::util::pycuda_get_ptr(filter);
//     long long ret_ptr = pfb::util::pycuda_get_ptr(ret);
//
//     cufftComplex* d_in = reinterpret_cast<cufftComplex*>(in_ptr);
//     cufftComplex* d_filter = reinterpret_cast<cufftComplex*>(filter_ptr);
//     cufftComplex* d_ret = reinterpret_cast<cufftComplex*>(ret_ptr);
//
//     $self->operator() (
//       d_in, d_filter, d_ret);
//     pfb::util::check_error("polyphase_filterbank 64");
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
//
// %extend pfb::PolyphaseFilterBankPlan<
//   cufftDoubleComplex, pfb::PolyphaseFilterBankEngineCUDA<cufftDoubleComplex>> {
//
//   PyObject* __call__ (PyObject* in, PyObject* filter, PyObject* ret)
//   {
//
//     long long in_ptr = pfb::util::pycuda_get_ptr(in);
//     long long filter_ptr = pfb::util::pycuda_get_ptr(filter);
//     long long ret_ptr = pfb::util::pycuda_get_ptr(ret);
//
//     cufftDoubleComplex* d_in = reinterpret_cast<cufftDoubleComplex*>(in_ptr);
//     cufftDoubleComplex* d_filter = reinterpret_cast<cufftDoubleComplex*>(filter_ptr);
//     cufftDoubleComplex* d_ret = reinterpret_cast<cufftDoubleComplex*>(ret_ptr);
//
//     $self->operator() (
//       d_in, d_filter, d_ret);
//     pfb::util::check_error("polyphase_filterbank 128");
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
//
//
//
//
//
//
// %inline %{
//
// namespace pfb {
//
//   PyObject* polyphase_analysis_in_place_cuda(
//     PyObject* in,
//     PyObject* filter,
//     PyObject* ret,
//     int channels,
//     const pfb::util::rational& os_factor
//   )
//   {
//
//     long long in_ptr = pfb::util::pycuda_get_ptr(in);
//     long in_dtype_num = pfb::util::pycuda_get_dtype_num(in);
//     unsigned in_size = pfb::util::pycuda_get_size(in);
//
//     long long filter_ptr = pfb::util::pycuda_get_ptr(filter);
//     long filter_dtype_num = pfb::util::pycuda_get_dtype_num(filter);
//     unsigned filter_size = pfb::util::pycuda_get_size(filter);
//
//     long long ret_ptr = pfb::util::pycuda_get_ptr(ret);
//     long ret_dtype_num = pfb::util::pycuda_get_dtype_num(ret);
//
//     if (in_dtype_num == NPY_COMPLEX64) {
//       typedef typename pfb::np2cufft<NPY_COMPLEX64>::type z_type;
//       z_type* d_in = reinterpret_cast<z_type*>(in_ptr);
//       z_type* d_filter = reinterpret_cast<z_type*>(filter_ptr);
//       z_type* d_ret = reinterpret_cast<z_type*>(ret_ptr);
//
//       pfb::PolyphaseFilterBankPlan<z_type, pfb::PolyphaseFilterBankEngineCUDA<z_type>> plan(
//         channels, os_factor, in_size, filter_size);
//
//       plan(
//         d_in, d_filter, d_ret);
//       pfb::util::check_error("polyphase_filterbank 64");
//
//     } else if (in_dtype_num == NPY_COMPLEX128) {
//       typedef typename pfb::np2cufft<NPY_COMPLEX128>::type z_type;
//       z_type* d_in = reinterpret_cast<z_type*>(in_ptr);
//       z_type* d_filter = reinterpret_cast<z_type*>(filter_ptr);
//       z_type* d_ret = reinterpret_cast<z_type*>(ret_ptr);
//
//       pfb::PolyphaseFilterBankPlan<z_type, pfb::PolyphaseFilterBankEngineCUDA<z_type>> plan(
//         channels, os_factor, in_size, filter_size);
//
//       plan(
//         d_in, d_filter, d_ret);
//       pfb::util::check_error("polyphase_filterbank 128");
//
//
//     } else {
//       PyErr_SetString(PyExc_ValueError, "Inputs need to be of NPY_COMPLEX64 or NPY_COMPLEX128 type");
//       return NULL;
//     }
//
//
//     Py_INCREF(Py_None);
//     return Py_None;
//
//   }
//
//
//   PyObject* polyphase_analysis_in_place_cpu(
//     PyObject* in,
//     PyObject* filter,
//     PyObject* ret,
//     int channels,
//     const pfb::util::rational& os_factor
//   )
//   {
//     PyArrayObject* ret_arr=NULL;
//     PyArrayObject* in_arr=NULL;
//     PyArrayObject* filter_arr=NULL;
//
//     const int dtype_in = PyArray_ObjectType(in, NPY_FLOAT);
//     const int dtype_filter = PyArray_ObjectType(filter, NPY_FLOAT);
//
//     in_arr = (PyArrayObject*) PyArray_FROM_OTF(in, dtype_in, NPY_ARRAY_IN_ARRAY);
//     filter_arr = (PyArrayObject*) PyArray_FROM_OTF(filter, dtype_filter, NPY_ARRAY_IN_ARRAY);
//     ret_arr = (PyArrayObject*) PyArray_FROM_OTF(ret, dtype_in, NPY_ARRAY_IN_ARRAY);
//
//     const int size_in = PyArray_DIMS(in_arr)[0];
//     const int size_filter = PyArray_DIMS(filter_arr)[0];
//
//     if (dtype_in == NPY_COMPLEX64) {
//       typedef typename pfb::np2std<NPY_COMPLEX64>::type z_type_8;
//
//       pfb::PolyphaseFilterBankPlan<z_type_8> plan(
//         channels, os_factor, size_in, size_filter
//       );
//
//       plan(
//         (z_type_8*) PyArray_BYTES(in_arr),
//         (z_type_8*) PyArray_BYTES(filter_arr),
//         (z_type_8*) PyArray_BYTES(ret_arr)
//       );
//
//     } else if (dtype_in == NPY_COMPLEX128) {
//       typedef typename pfb::np2std<NPY_COMPLEX128>::type z_type_16;
//
//       pfb::PolyphaseFilterBankPlan<z_type_16> plan(
//         channels, os_factor, size_in, size_filter
//       );
//       plan(
//         (z_type_16*) PyArray_BYTES(in_arr),
//         (z_type_16*) PyArray_BYTES(filter_arr),
//         (z_type_16*) PyArray_BYTES(ret_arr)
//       );
//
//     } else {
//       PyErr_SetString(PyExc_ValueError, "Inputs need to be of NPY_COMPLEX64 or NPY_COMPLEX128 type");
//       return NULL;
//     }
//
//     Py_INCREF(Py_None);
//     return Py_None;
//   }
// }
// %}
%pythoncode "psr_gen_api.py"
