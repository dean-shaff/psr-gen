#ifndef __python_util_hpp
#define __python_util_hpp


#include "numpy/noprefix.h"
#include "numpy/arrayobject.h"

namespace psr_gen {
namespace util {

  template<int T>
  struct np2std;

  template<>
  struct np2std<NPY_FLOAT32> {
    typedef float type;
  };

  template<>
  struct np2std<NPY_FLOAT64> {
    typedef double type;
  };


  PyArrayObject* numpy_get_arr_from_obj (PyObject* obj) {
    const int dtype_obj = PyArray_ObjectType(obj, NPY_FLOAT);
    return (PyArrayObject*) PyArray_FROM_OTF(obj, dtype_obj, NPY_ARRAY_IN_ARRAY);
  }

  long pycuda_get_ptr (PyObject* obj) {
    return PyLong_AsLongLong(
      PyObject_GetAttrString(obj, "ptr")
    );
  }

  long long pycuda_get_dtype_num (PyObject* obj) {
    return PyLong_AsLong(
        PyObject_GetAttrString(
            PyObject_GetAttrString(obj, "dtype"),
            "num"));
  }

  long long pycuda_get_size (PyObject* obj) {
    return (unsigned) PyLong_AsLong(
        PyTuple_GetItem(
          PyObject_GetAttrString(obj, "shape"), 0));
  }

}
}
#endif
