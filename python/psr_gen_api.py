import typing

import numpy as np
import pycuda.gpuarray as gpuarray


__all__ = [
    "PulsarGenerator"
]


array_type = typing.Union[np.array, gpuarray.GPUArray]


class PulsarGenerator:

    constructors = {
        np.float32: {
            "cuda": PulsarGeneratorFloatCUDA,
            "openmp": PulsarGeneratorFloatOpenMP
        },
        np.float64: {
            "cuda": PulsarGeneratorDoubleCUDA,
            "openmp": PulsarGeneratorDoubleOpenMP
        }
    }

    def __init__(
        self,
        bandwidth: float,
        centre_freq: float,
        npol: int,
        DM: float,
        period: float,
        noise: float,
        len_fft: int,
        phase_bins: int,
        dtype: np.dtype = np.float32,
        use_gpu: bool = False
    ):
        self._bandwidth = bandwidth
        self._centre_freq = centre_freq
        self._npol = npol
        self._DM = DM
        self._period = period
        self._noise = noise
        self._len_fft = len_fft
        self._phase_bins = phase_bins
        self._dtype = dtype
        self._use_gpu = use_gpu
        self._obj = self._init_obj(
            bandwidth, centre_freq, npol, DM, period, noise, len_fft, phase_bins)

    def __call__(self, seconds: float) -> array_type:
        pass

    def _init_obj(self, *args):

        engine_str = "cuda" if self.use_gpu else "openmp"

        constructor = self._constructors[self._dtype][engine_str]

        self._obj = constructor(*args)
