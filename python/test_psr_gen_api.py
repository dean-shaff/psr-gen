import os
import sys
import unittest

import numpy as np

python_dir = os.path.dirname(os.path.abspath(__file__))
build_dir = os.path.join(
    os.path.dirname(python_dir),
    "build"
)

sys.path.append(os.path.join(build_dir, "python"))

import psr_gen


class TestPulsarGenerator(unittest.TestCase):

    def setUp(self):
        pass

    def test_init(self):

        for dtype in [np.float32, np.float64]:
            for use_gpu in [True, False]:
                psr_gen.PulsarGenerator(
                    40., 1405., 2, 0.1, 0.005, 0.1, 2048, 512,
                    use_gpu=use_gpu, dtype=dtype)

    def test_call(self):
        pass
