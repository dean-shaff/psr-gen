#include <vector>

#include "catch.hpp"

#include "psr_gen/util/util.hpp"
#include "util.hpp"

// TEMPLATE_TEST_CASE(
//   "matrix multiplication works with sub matrices",
//   "[unit][util]",
//   float, double
// )
// {
//   std::vector<TestType> A = {
//     0.05890259, 0.62854674,
//     0.49068355, 0.19632523,
//     0.13256636, 0.24694413,
//     0.41307086, 0.32871844,
//     0.28652921, 0.72276439
//   };
//
//   std::vector<TestType> B = {
//     0.46591739, 0.46366204,
//     0.65801672, 0.53256443
//   };
//
//   std::vector<TestType> C_expected = {
//     0.44103801, 0.36205253,
//     0.35780328, 0.33206717,
//     0.22425834, 0.19297965
//   };
//
//   std::vector<TestType> C(5*2, 0.0);
//
//   psr_gen::util::matmul(A.data(), {3, 2}, B.data(), {2, 2}, C.data());
//
//   std::vector<TestType> C_test(C.begin(), C.begin() + 6);
//
//   unsigned nclose = test::util::nclose(C_test, C_expected);
//
//   // for (unsigned idx=0; idx<C.size(); idx++) {
//   //   std::cerr << C[idx] << " ";
//   // }
//   // std::cerr << std::endl;
//
//
//   REQUIRE(nclose == C_test.size());
// }
//
//
//
// TEMPLATE_TEST_CASE(
//   "matrix multiplication works",
//   "[unit][util]",
//   float, double
// )
// {
//   std::vector<TestType> A_m = {
//     0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11
//   };
//
//   std::vector<TestType> B_m = {
//     0,  1,  2,  3,  4,  5
//   };
//
//   std::vector<TestType> C_m(2*4, 0.0);
//   std::vector<TestType> C_expected = {
//     10, 13, 28, 40, 46, 67, 64, 94
//   };
//
//   psr_gen::util::matmul(A_m.data(), {4, 3}, B_m.data(), {3, 2}, C_m.data());
//
//   unsigned nclose = test::util::nclose(C_m, C_expected);
//
//   REQUIRE(nclose == C_m.size());
// }
//
// TEMPLATE_TEST_CASE(
//   "complex matrix multiplication works",
//   "[unit][util]",
//   float, double
// )
// {
//   typedef std::complex<TestType> z_type;
//
//   std::vector<z_type> A_m (12);
//   std::vector<z_type> B_m (6);
//
//   for (unsigned idx=0; idx<A_m.size(); idx++) {
//     A_m[idx] = z_type((TestType) idx, (TestType) (idx + 1));
//     if (idx < B_m.size()) {
//       B_m[idx] = z_type((TestType) idx, (TestType) (idx + 1));
//     }
//   }
//
//   std::vector<z_type> C_m (2*4);
//
//   std::vector<z_type> C_expected_z = {
//     -12.0 +29.0i, -15.0 +38.0i,
//     -21.0 +74.0i, -24.0+101.0i,
//     -30.0+119.0i, -33.0+164.0i,
//     -39.0+164.0i, -42.0+227.0i
//   };
//
//   psr_gen::util::matmul(A_m.data(), {4, 3}, B_m.data(), {3, 2}, C_m.data());
//
//   unsigned nclose = test::util::nclose(C_m, C_expected_z);
//
//   // for (unsigned idx=0; idx<C_expected_z.size(); idx++) {
//   //   std::cerr << C_m[idx] << " ";
//   // }
//   // std::cerr << std::endl;
//
//   REQUIRE(nclose == C_m.size());
// }
