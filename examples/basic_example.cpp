#include <vector>

#include "psr_gen/PulsarGenerator.hpp"

int main ()
{
  psr_gen::PulsarGenerator<float> gen(
    5, // bandwidth
    1405, // centre_freq
    2, // npol
    0.1, // DM
    0.00575745, // period
    0.8, // noise
    32768, // len_fft
    1024 // phase_bins
  );

  std::vector<std::complex<float>> out;
  gen.compute(0.1, out); // compute one second worth of data.

  return 0;
}
