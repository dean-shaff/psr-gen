// ./bench_PulsarGeneratorEngineOpenMP -l 1048576 -p 1024 -t 0.1
#include <iostream>

#include "psr_gen/config.hpp"
#include "psr_gen/PulsarGenerator.hpp"
#include "psr_gen/PulsarGeneratorEngineOpenMP.hpp"
#include "psr_gen/util/util.hpp"

#if HAVE_CUDA
#include "psr_gen/PulsarGeneratorEngineCUDA_wrapper.hpp"
#endif

#include "create_parser.hpp"

int main (int argc, char *argv[]) {
  cxxopts::Options options(
    "bench_PulsarGenerator",
    "Benchmark Pulsar Generator");

  psr_gen::internal::create_parser(options);
  auto result = options.parse(argc, argv);

  if (result["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }

  psr_gen::config::verbose() = result["verbose"].as<bool>();

  auto t0 = psr_gen::util::now();
  unsigned len_fft = result["len_fft"].as<unsigned>();
  unsigned phase_bins = result["phase_bins"].as<unsigned>();
  double sec = result["time"].as<double>();

  #if HAVE_CUDA
  if (result["gpu"].as<bool>()) {
    psr_gen::PulsarGenerator<
      float,
      psr_gen::PulsarGeneratorEngineCUDA<float>
    > gen(
      result["bandwidth"].as<double>(),
      result["centre_freq"].as<double>(),
      2,
      result["DM"].as<double>(),
      result["period"].as<double>(),
      result["noise"].as<double>(),
      len_fft,
      phase_bins);

    psr_gen::PulsarGenerator_operate(gen, sec);
  } else
  #endif
  {
    psr_gen::PulsarGenerator<float> gen(
      result["bandwidth"].as<double>(),
      result["centre_freq"].as<double>(),
      2,
      result["DM"].as<double>(),
      result["period"].as<double>(),
      result["noise"].as<double>(),
      len_fft,
      phase_bins);

    std::vector<std::complex<float>> out;
    gen.compute(sec, out);
  }

  psr_gen::util::delta<std::milli> elapsed = psr_gen::util::now() - t0;

  std::cerr << "Took " << elapsed.count() << " ms to generate "
    << sec << " s of data" << std::endl;
}
