#include <exception>

#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/execution_policy.h>

#include "psr_gen/config.hpp"
#include "psr_gen/util/util.hpp"
#include "psr_gen/util/findphase.hpp"
#include "psr_gen/util/cholesky_2d.hpp"
#include "psr_gen/PulsarGeneratorEngineCUDA.hpp"

#include "psr_gen/util/cuda/util.cuh"
#include "psr_gen/util/cuda/PulsarGeneratorEngineCUDA_kernels.cuh"

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess)
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__global__ void psr_gen::util::cuda::init_rand(
  curandState* states,
  unsigned max_offset,
  unsigned seed
)
{
  curand_init(seed, threadIdx.x*max_offset, 0, &states[threadIdx.x]);
}


namespace psr_gen {

template<typename T>
using complex_type = typename psr_gen::util::cuda::cufft_type_map<T>::complex_type;

template<typename T>
PulsarGeneratorEngineCUDA<T>::PulsarGeneratorEngineCUDA (
  unsigned _npol,
  double _period,
  unsigned _len_fft,
  unsigned _n_disp_low,
  unsigned _n_disp_high,
  unsigned _phase_bins,
  double _sampling_rate,
  const std::vector<std::vector<std::complex<T>>>& _coherency,
  const std::vector<std::complex<double>>& _dispvec
) :
  npol(_npol), period(_period), len_fft(_len_fft), n_disp_low(_n_disp_low),
  n_disp_high(_n_disp_high), phase_bins(_phase_bins),
  sampling_rate(_sampling_rate), coherency(_coherency), dispvec(_dispvec)
{
  nclip = len_fft - (n_disp_low + n_disp_high);

  int rank = 1; // 1D transform
  int n[] = {(int) len_fft}; /* 1d transforms of length channels */
  int howmany = (int) npol;
  int idist = 1;
  int odist = 1;
  int istride = howmany;
  int ostride = howmany;
  int *inembed = n;
  int *onembed = n;

  cufftResult success = cufftPlanMany(
    &plan, rank, n,
    inembed, istride, idist,
    onembed, ostride, odist,
    psr_gen::util::cuda::cufft_type_map<complex_type<T>>::flag, howmany);

}

template<typename T>
template<typename ContainerType>
void PulsarGeneratorEngineCUDA<T>::compute (
  unsigned nblocks, ContainerType& out
)
{
  compute(nblocks, thrust::raw_pointer_cast(&out[0]));
}

template<typename T>
void PulsarGeneratorEngineCUDA<T>::compute (
  unsigned nblocks, std::complex<T>* out
)
{
  // thrust::device_pointer_cast(&a[0]);
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::PulsarGeneratorEngineCUDA::compute" << std::endl;
  }
  thrust::device_vector<std::complex<T>> coherency_device(4*len_fft);
  thrust::device_vector<std::complex<T>> dispersion_device(len_fft);

  for (unsigned idx=0; idx<coherency.size(); idx++) {
    thrust::copy(
      coherency[idx].begin(),
      coherency[idx].end(),
      coherency_device.begin() + idx*len_fft);
  }

  std::vector<std::complex<T>> _dispvec(dispvec.begin(), dispvec.end());

  thrust::copy(
    _dispvec.begin(),
    _dispvec.end(),
    dispersion_device.begin());

  thrust::device_vector<T> phase_vec_device(len_fft);
  thrust::device_vector<complex_type<T>> pulsar_vec_device(len_fft*npol);
  thrust::device_vector<T> indices_device(len_fft);


  dim3 grid_siftphases{1, 1, 1};
  dim3 grid_populatephases{len_fft/1024, npol, 1};
  if (grid_populatephases.x == 0) {
    grid_populatephases.x++;
  }
  dim3 grid_saveoverlap{nclip/1024, 2, 1};
  if (grid_saveoverlap.x == 0) {
    grid_saveoverlap.x++;
  }

  dim3 threads{len_fft < 1024 ? len_fft: 1024, 1, 1};
  dim3 threads_saveoverlap{nclip < 1024 ? nclip: 1024, 1, 1};

  curandState* drand_states;
  cudaMalloc((void **)&drand_states, threads.x * sizeof(curandState));
  unsigned seed = 1234;
  unsigned max_offset = nblocks*grid_populatephases.x*npol;
  psr_gen::util::cuda::init_rand<<<1, threads>>>(
    drand_states, max_offset, seed);

  unsigned indices_device_size;

  for (unsigned iblock=0; iblock<nblocks; iblock++) {

    psr_gen::util::cuda::populate_phase<T><<<grid_populatephases, threads>>>(
      thrust::raw_pointer_cast(&phase_vec_device[0]),
      len_fft,
      sampling_rate,
      phase_bins,
      period,
      n_disp_high,
      iblock);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    auto indices_device_end = thrust::unique_copy(
      thrust::device,
      phase_vec_device.begin(),
      phase_vec_device.end(),
      indices_device.begin());

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    indices_device_size = indices_device_end - indices_device.begin();
    grid_siftphases.x = indices_device_size;

    size_t shared_bytes = 4*sizeof(complex_type<T>);
    if (psr_gen::config::verbose()) {
      std::cerr << "psr_gen::PulsarGeneratorEngineCUDA::compute: indices_device_size="
        << indices_device_size << std::endl;
      std::cerr << "psr_gen::PulsarGeneratorEngineCUDA::compute: iblock=" << iblock << std::endl;
      std::cerr << "psr_gen::PulsarGeneratorEngineCUDA::compute: shared_bytes=" << shared_bytes << std::endl;
    }

    psr_gen::util::cuda::sift_phases<T><<<grid_siftphases, threads, shared_bytes>>>(
      thrust::raw_pointer_cast(&indices_device[0]),
      indices_device_size,
      thrust::raw_pointer_cast(&phase_vec_device[0]),
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      (complex_type<T>*) thrust::raw_pointer_cast(&coherency_device[0]),
      npol,
      len_fft,
      drand_states
    );
    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    cufftResult success = psr_gen::util::cuda::cufft_type_map<complex_type<T>>::execute(
      plan,
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      CUFFT_FORWARD);

    if (success != CUFFT_SUCCESS) {
      throw std::runtime_error("Couldn't execute forward FFT plan");
    }

    psr_gen::util::cuda::multiply_dispersion_kernel<T><<<grid_populatephases, threads>>>(
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      (complex_type<T>*) thrust::raw_pointer_cast(&dispersion_device[0]),
      npol,
      len_fft);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

    success = psr_gen::util::cuda::cufft_type_map<complex_type<T>>::execute(
      plan,
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      CUFFT_INVERSE);

    if (success != CUFFT_SUCCESS) {
      throw std::runtime_error("Couldn't execute backward FFT plan");
    }

    psr_gen::util::cuda::save_overlap<T><<<grid_saveoverlap, threads_saveoverlap>>>(
      thrust::raw_pointer_cast(&pulsar_vec_device[0]),
      ((complex_type<T>*) out) + iblock*nclip*npol,
      psr_gen::util::cuda::cufft_type_map<complex_type<T>>::init(1.0/len_fft),
      // psr_gen::util::cuda::cufft_type_map<complex_type<T>>::init(1.0),
      npol,
      nclip,
      n_disp_high);

    cudaDeviceSynchronize();
    gpuErrchk(cudaPeekAtLastError());

  }

  cudaFree(drand_states);
}


template class PulsarGeneratorEngineCUDA<float>;
template void PulsarGeneratorEngineCUDA<float>::compute<
  thrust::device_vector<std::complex<float>>
>(unsigned, thrust::device_vector<std::complex<float>>&);

template class PulsarGeneratorEngineCUDA<double>;
template void PulsarGeneratorEngineCUDA<double>::compute<
  thrust::device_vector<std::complex<double>>
>(unsigned, thrust::device_vector<std::complex<double>>&);

}
