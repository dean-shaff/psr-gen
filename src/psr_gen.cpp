#include <map>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <cstring>

#include "psr_gen/config.hpp"
#include "psr_gen/util/util.hpp"
#include "psr_gen/PulsarGeneratorEngineOpenMP.hpp"
#include "psr_gen/PulsarGenerator.hpp"

#if HAVE_CUDA
#include "psr_gen/PulsarGeneratorEngineCUDA_wrapper.hpp"
#endif

#include "create_parser.hpp"


template<typename MapType>
std::string header_to_str(MapType& header_map)
{
  std::stringstream sstr;

  int header_size = std::stoi(header_map["HDR_SIZE"]);

  for (auto it=header_map.begin(); it != header_map.end(); it++) {
    sstr << it->first << " " << it->second << "\n";
  }

  int bytes_remaining = header_size - sstr.str().size();

  for (int idx=0; idx<bytes_remaining; idx++) {
    sstr << '\0';
  }

  return sstr.str();
}

template<typename T>
void write_to_file (std::ofstream& file, const std::vector<T>& data)
{
  file.write(
    reinterpret_cast<const char*>(data.data()),
    data.size()*sizeof(T)
  );
}

template<typename PulsarGeneratorType, typename MapType>
void update_header(const PulsarGeneratorType& gen, MapType& header_map) {
  header_map["BW"] = std::to_string(gen.get_bandwidth());
  header_map["FREQ"] = std::to_string(gen.get_centre_freq());
  header_map["TSAMP"] = std::to_string(gen.get_sampling_rate() * 1e6);
  header_map["DM"] = std::to_string(gen.get_DM());
  header_map["PERIOD"] = std::to_string(gen.get_period());
}



int main (int argc, char *argv[]) {
  std::map<std::string, std::string> header_map {
    {"HDR_SIZE", "4096"},
    {"BW", "40"},
    {"DEC", "-45:59:09.5"},
    {"DSB", "0"},
    {"FREQ", "1405.000000"},
    {"HDR_VERSION", "1.0"},
    {"INSTRUMENT", "dspsr"},
    {"MODE", "PSR"},
    {"NBIT", "32"},
    {"NCHAN", "1"},
    {"NDIM", "2"},
    {"NPOL", "2"},
    {"OBS_OFFSET", "0"},
    {"PRIMARY", "dspsr"},
    {"RA", "16:44:49.28"},
    {"SOURCE", "J1644-4559"},
    {"TELESCOPE", "PKS"},
    {"TSAMP", "0.025"},
    {"UTC_START", "2019-02-05-01:15:49"}
  };
  cxxopts::Options options(
    "psr_gen",
    "Write simulated pulsar data to DADA file");

  options.add_options()
    ("file_name", "Output filename", cxxopts::value<std::string>()->default_value("simulated_pulsar.dump"));

  psr_gen::internal::create_parser(options);

  auto result = options.parse(argc, argv);

  if (result["help"].as<bool>()) {
    std::cerr << options.help() << std::endl;
    return 0;
  }

  psr_gen::config::verbose() = result["verbose"].as<bool>();

  unsigned len_fft = result["len_fft"].as<unsigned>();
  unsigned phase_bins = result["phase_bins"].as<unsigned>();
  double sec = result["time"].as<double>();

  std::vector<std::complex<float>> out;

  #if HAVE_CUDA
  if (result["gpu"].as<bool>()) {
    if (psr_gen::config::verbose()) {
      std::cerr << "using GPU engine" << std::endl;
    }

    psr_gen::PulsarGenerator<
      float,
      psr_gen::PulsarGeneratorEngineCUDA<float>
    > gen(
      result["bandwidth"].as<double>(),
      result["centre_freq"].as<double>(),
      2,
      result["DM"].as<double>(),
      result["period"].as<double>(),
      result["noise"].as<double>(),
      len_fft,
      phase_bins);

    psr_gen::PulsarGenerator_operate(gen, sec, out);
    update_header(gen, header_map);
  } else
  #endif
  {
    psr_gen::PulsarGenerator<float> gen(
      result["bandwidth"].as<double>(),
      result["centre_freq"].as<double>(),
      2,
      result["DM"].as<double>(),
      result["period"].as<double>(),
      result["noise"].as<double>(),
      len_fft,
      phase_bins);

    gen.compute(sec, out);
    update_header(gen, header_map);
  }


  std::string header_str = header_to_str(header_map);
  std::string file_name = result["file_name"].as<std::string>();

  auto t0 = psr_gen::util::now();
  std::ofstream file(file_name, std::ios::out | std::ios::binary);

  file.write(
    header_str.c_str(),
    header_str.size());

  write_to_file(file, out);

  psr_gen::util::delta<std::milli> delta = psr_gen::util::now() - t0;

  if (psr_gen::config::verbose()) {
    std::cerr << "Took " << delta.count() << " ms to write to disk" << std::endl;
  }

}
