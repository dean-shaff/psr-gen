#include <complex>

#include <thrust/device_vector.h>

#include "psr_gen/PulsarGeneratorEngineCUDA_wrapper.hpp"

namespace psr_gen {

template<typename Type>
void PulsarGenerator_operate (PulsarGeneratorCUDA<Type>& gen, double time)
{
  thrust::device_vector<std::complex<Type>> out;
  gen.compute(time, out);
}

template void PulsarGenerator_operate<float>(PulsarGeneratorCUDA<float>& gen, double time);
template void PulsarGenerator_operate<double>(PulsarGeneratorCUDA<double>& gen, double time);

template<typename Type>
void PulsarGenerator_operate (
  PulsarGeneratorCUDA<Type>& gen,
  double time,
  std::vector<std::complex<Type>>& out
)
{
  thrust::device_vector<std::complex<Type>> out_cuda;
  gen.compute(time, out_cuda);
  out.resize(out_cuda.size());
  thrust::copy(
    out_cuda.begin(),
    out_cuda.end(),
    out.begin());
}

template void PulsarGenerator_operate<float>(PulsarGeneratorCUDA<float>& gen, double time, std::vector<std::complex<float>>& out);
template void PulsarGenerator_operate<double>(PulsarGeneratorCUDA<double>& gen, double time, std::vector<std::complex<double>>& out);

template<typename Type>
void PulsarGenerator_operate (
  PulsarGeneratorCUDA<Type>& gen,
  double time,
  std::complex<Type>* out
)
{
  gen.compute(time, out);
}

template void PulsarGenerator_operate<float>(PulsarGeneratorCUDA<float>& gen, double time, std::complex<float>* out);
template void PulsarGenerator_operate<double>(PulsarGeneratorCUDA<double>& gen, double time, std::complex<double>* out);



}
