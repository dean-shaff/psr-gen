#include "omp.h"

#include "psr_gen/config.hpp"
#include "psr_gen/util/findphase.hpp"
#include "psr_gen/util/cholesky_2d.hpp"
#include "psr_gen/PulsarGeneratorEngineOpenMP.hpp"

namespace psr_gen {

template<typename T>
PulsarGeneratorEngineOpenMP<T>::PulsarGeneratorEngineOpenMP (
  unsigned _npol,
  double _period,
  unsigned _len_fft,
  unsigned _n_disp_low,
  unsigned _n_disp_high,
  unsigned _phase_bins,
  double _sampling_rate,
  const std::vector<std::vector<std::complex<T>>>& _coherency,
  const std::vector<std::complex<double>>& _dispvec
) :
  npol(_npol), period(_period), len_fft(_len_fft), n_disp_low(_n_disp_low),
  n_disp_high(_n_disp_high), phase_bins(_phase_bins),
  sampling_rate(_sampling_rate), coherency(_coherency), dispvec(_dispvec)
{
  distribution = std::normal_distribution<T>(0.0, 1.0);
  nclip = len_fft - (n_disp_low + n_disp_high);
}

template<typename T>
template<typename ContainerType>
void PulsarGeneratorEngineOpenMP<T>::compute (
  unsigned nblocks, ContainerType& out
)
{
  compute(nblocks, out.data());
}


template<typename T>
void PulsarGeneratorEngineOpenMP<T>::compute (
  unsigned nblocks, std::complex<T>* out
)
{
  // from signalgen.m:
  // (1.0/bandwidth)*1e-6 is sampling_period, or Tin
  // len_fft is Nout, or Nin
  // phase_bins is nbins
  // t0 is 0.0, or the absolute start time

  int rank = 1; // 1D transform
  int n[] = {(int) len_fft}; /* 1d transforms of length channels */
  int howmany = (int) npol;
  int idist = 1;
  int odist = 1;
  int istride = npol;
  int ostride = npol;
  int *inembed = n;
  int *onembed = n;

  std::vector<T> phase_vec(len_fft);
  std::vector<std::complex<T>> pulsar_vec(
    len_fft*npol, std::complex<T>(0.0, 0.0));

  fftw3_type* fft_data = reinterpret_cast<fftw3_type*>(pulsar_vec.data());

  // auto t0 = psr_gen::util::now();
  // psr_gen::util::delta<std::milli> delta0;

  fftw3_plan_type plan_forward = fftw3_type_map::make_plan_many(
    rank, n, howmany,
    fft_data, inembed, istride, idist,
    fft_data, onembed, ostride, odist,
    FFTW_FORWARD, FFTW_ESTIMATE);

  fftw3_plan_type plan_backward = fftw3_type_map::make_plan_many(
    rank, n, howmany,
    fft_data, inembed, istride, idist,
    fft_data, onembed, ostride, odist,
    FFTW_BACKWARD, FFTW_ESTIMATE);

  // delta0 = psr_gen::util::now() - t0;

  // if (psr_gen::config::verbose()) {
  //   std::cerr << "Took " << delta0.count() << " ms to construct FFT plans" << std::endl;
  // }

  std::vector<T> indices;
  int max_threads = omp_get_max_threads();

  std::vector<std::vector<std::complex<T>>> random_phasors(
    max_threads, std::vector<std::complex<T>>(npol));
  std::vector<std::vector<std::complex<T>>> phase_coherency_matrices(
    max_threads, std::vector<std::complex<T>>(4));


  // std::vector<std::complex<T>> random_phasor(npol, std::complex<T>(0.0, 0.0));
  // std::vector<std::complex<T>> phase_coherency_matrix(4);

  // std::vector<std::complex<T>> random_phasors(npol*phase_bins, std::complex<T>(0.0, 0.0));
  // std::vector<std::complex<T>> chol_phasor_result(npol*phase_bins, std::complex<T>(0.0, 0.0)); // this is where the matrix multiplication between cholesky decomposition and random_phasors is stored

  for (unsigned iblock=0; iblock<nblocks; iblock++) {

    // t0 = psr_gen::util::now();
    T tval;
    unsigned nclip_high = len_fft - 2*n_disp_high;
    for (unsigned idx=0; idx<len_fft; idx++) {
      if (iblock == 0) {
        tval = ((T) idx - n_disp_high)*sampling_rate;
      } else {
        tval = ((T) (iblock*(nclip_high) + idx - n_disp_high)) * sampling_rate;
      }
      phase_vec[idx] = psr_gen::util::findphase(tval, phase_bins, period);
    }
    psr_gen::util::unique_sort(phase_vec, indices);
    // delta0 = psr_gen::util::now() - t0;


    // if (psr_gen::config::verbose()) {
    //   std::cerr << "iblock=" << iblock << " : Took " << delta0.count() << " ms to find phase and sort" << std::endl;
    // }

    // const double delta = 1.0 / ((double) phase_bins - 1);
    #if TEST_MODE == 0
    const double sqrt_half = std::sqrt(0.5);
    #endif
    // t0 = psr_gen::util::now();

    #pragma omp parallel for
    for (unsigned idx=0; idx<indices.size(); idx++) {

      int thread_idx = omp_get_thread_num();
      T T_index = indices[idx];
      unsigned index = (unsigned) T_index;


      phase_coherency_matrices[thread_idx][0] = coherency[0][index];
      phase_coherency_matrices[thread_idx][1] = coherency[1][index];
      phase_coherency_matrices[thread_idx][2] = coherency[2][index];
      phase_coherency_matrices[thread_idx][3] = coherency[3][index];

      psr_gen::util::cholesky_2d<std::complex<T>>(
        phase_coherency_matrices[thread_idx].data(), "u");

      unsigned counter = 0;
      for (unsigned iphase=0; iphase<phase_vec.size(); iphase++) {
        if (phase_vec[iphase] == T_index) {

          for (unsigned ipol=0; ipol<npol; ipol++) {
          #if TEST_MODE == 1
            // random_phasors[thread_idx][ipol] = std::complex<T>(
            //     sqrt_half*delta*(T)counter,
            //     sqrt_half*delta*(T)counter
            // );
            random_phasors[thread_idx][ipol] = std::complex<T>(
                (T)iphase,
                (T)iphase
            );
          #else
            random_phasors[thread_idx][ipol] = std::complex<T>(
              sqrt_half*distribution(generator),
              sqrt_half*distribution(generator)
            );

          #endif
          }
          for (unsigned ipol=0; ipol<npol; ipol++) {
            pulsar_vec[npol*iphase + ipol] = random_phasors[thread_idx][0]*phase_coherency_matrices[thread_idx][ipol] +
                                             random_phasors[thread_idx][1]*phase_coherency_matrices[thread_idx][npol + ipol];
          }
          counter++;
        }
      }
    }

    // delta0 = psr_gen::util::now() - t0;
    // if (psr_gen::config::verbose()) {
    //   std::cerr << "iblock="<< iblock <<" : Took " << delta0.count() << " ms to sift phases" << std::endl;
    // }
    // t0 = psr_gen::util::now();

    fftw3_type_map::execute_plan(plan_forward);

    // multiply by dispersion kernel
    for (unsigned idx=0; idx<len_fft; idx++) {
      for (unsigned ipol=0; ipol<npol; ipol++) {
        pulsar_vec[npol*idx + ipol] *= dispvec[idx];
      }
    }
    fftw3_type_map::execute_plan(plan_backward);
    // delta0 = psr_gen::util::now() - t0;
    //
    // if (psr_gen::config::verbose()) {
    //   std::cerr << "iblock="<< iblock <<" : Took " << delta0.count() << " ms to do FFTs" << std::endl;
    // }
    // t0 = psr_gen::util::now();
    std::complex<T> z_len_fft ((T) len_fft, 0.0);

    // remove convolution overlap regions, calculated in dedispersion kernel
    // FFTW doesn't do any scaling in the ifft, so scale result appropriately as well
    for (unsigned idx=0; idx<nclip; idx++){
      for (unsigned ipol=0; ipol<npol; ipol++) {
        out[iblock*nclip*npol + idx*npol + ipol] = pulsar_vec[npol*(idx+n_disp_high) + ipol] / z_len_fft;
      }
    }
    // delta0 = psr_gen::util::now() - t0;
    // if (psr_gen::config::verbose()) {
    //   std::cerr << "iblock="<< iblock <<" : Took " << delta0.count() << " ms to save data" << std::endl;
    // }
  }
}



template class PulsarGeneratorEngineOpenMP<float>;
template void PulsarGeneratorEngineOpenMP<float>::compute<std::vector<std::complex<float>>>(
  unsigned, std::vector<std::complex<float>>&);

template class PulsarGeneratorEngineOpenMP<double>;
template void PulsarGeneratorEngineOpenMP<double>::compute<std::vector<std::complex<double>>>(
  unsigned, std::vector<std::complex<double>>&);

}
