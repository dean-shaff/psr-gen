#include "cxxopts.hpp"

namespace psr_gen {
namespace internal {
  inline void create_parser (cxxopts::Options& options)
  {
    options.add_options()
      ("v,verbose", "Verbose output")
      ("h,help", "Help")
      ("t,time", "time", cxxopts::value<double>())
      ("n,noise", "noise parameter", cxxopts::value<double>()->default_value("0.0"))
      ("D,DM", "dispersion measure", cxxopts::value<double>()->default_value("2.64476"))
      ("c,period", "pulsar period", cxxopts::value<double>()->default_value("0.00575745"))
      ("b,bandwidth", "bandwidth", cxxopts::value<double>()->default_value("40"))
      ("f,centre_freq", "centre frequency", cxxopts::value<double>()->default_value("1405.0"))
      ("l,len_fft", "Length of input FFT", cxxopts::value<unsigned>()->default_value("1048576"))
      ("p,phase_bins", "Pulsar phase bins", cxxopts::value<unsigned>()->default_value("1024"))
      ("gpu", "Use GPU engine")
      ;
  }
}
}
