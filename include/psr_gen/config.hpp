#ifndef __psr_gen_config_hpp
#define __psr_gen_config_hpp

namespace psr_gen {
struct config {

  static bool& verbose ()
  {
    static bool _verbose = 0;
    return _verbose;
  }
};
}

#endif
