#ifndef __PulsarGeneratorEngineCUDA_cuh
#define __PulsarGeneratorEngineCUDA_cuh

#include <vector>
#include <complex>

#include <cufft.h>

#include "PulsarGeneratorEngine.hpp"

#ifndef TEST_MODE
#define TEST_MODE 0
#endif

namespace psr_gen {

template<typename T>
class PulsarGeneratorEngineCUDA : public psr_gen::PulsarGeneratorEngine<T> {

public:

  PulsarGeneratorEngineCUDA(
    unsigned _npol,
    double _period,
    unsigned _len_fft,
    unsigned _n_disp_low,
    unsigned _n_disp_high,
    unsigned _phase_bins,
    double _sampling_rate,
    const std::vector<std::vector<std::complex<T>>>& _coherency,
    const std::vector<std::complex<double>>& _dispvec);

  template<typename ContainerType>
  void compute(unsigned nblocks, ContainerType& out);

  void compute(unsigned nblocks, std::complex<T>* out);

protected:
  unsigned npol;
  double period;

  unsigned len_fft;
  unsigned n_disp_low;
  unsigned n_disp_high;
  unsigned nclip;

  unsigned phase_bins;

  double sampling_rate;

  std::vector<std::vector<std::complex<T>>> coherency;
  std::vector<std::complex<double>> dispvec;

private:

  cufftHandle plan;

};
}


#endif
