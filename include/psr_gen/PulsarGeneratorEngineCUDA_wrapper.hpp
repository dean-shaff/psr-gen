#ifndef __PulsarGeneratorEngineCUDA_wrapper_hpp
#define __PulsarGeneratorEngineCUDA_wrapper_hpp

#include "psr_gen/PulsarGenerator.hpp"
#include "psr_gen/PulsarGeneratorEngineCUDA.hpp"

namespace psr_gen {

template<typename Type>
using PulsarGeneratorCUDA = psr_gen::PulsarGenerator<
  Type, psr_gen::PulsarGeneratorEngineCUDA<Type>>;

template<typename Type>
void PulsarGenerator_operate (
  PulsarGeneratorCUDA<Type>& gen,
  double time);

template<typename Type>
void PulsarGenerator_operate (
  PulsarGeneratorCUDA<Type>& gen,
  double time,
  std::vector<std::complex<Type>>& out);

template<typename Type>
void PulsarGenerator_operate (
  PulsarGeneratorCUDA<Type>& gen,
  double time,
  std::complex<Type>* out);

}


#endif
