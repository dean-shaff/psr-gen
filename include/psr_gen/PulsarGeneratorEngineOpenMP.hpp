#ifndef __PulsarGeneratorEngineOpenMP_hpp
#define __PulsarGeneratorEngineOpenMP_hpp

#include <random>
#include <vector>
#include <complex>

#include "psr_gen/util/util.hpp"
#include "psr_gen/PulsarGeneratorEngine.hpp"

#ifndef TEST_MODE
#define TEST_MODE 0
#endif

namespace psr_gen {

template<typename T>
class PulsarGeneratorEngineOpenMP : public PulsarGeneratorEngine<T> {

  typedef typename psr_gen::util::fftw3_type_map<std::complex<T>> fftw3_type_map;
  typedef typename fftw3_type_map::type fftw3_type;
  typedef typename fftw3_type_map::plan_type fftw3_plan_type;

public:

  PulsarGeneratorEngineOpenMP(
    unsigned _npol,
    double _period,
    unsigned _len_fft,
    unsigned _n_disp_low,
    unsigned _n_disp_high,
    unsigned _phase_bins,
    double _sampling_rate,
    const std::vector<std::vector<std::complex<T>>>& _coherency,
    const std::vector<std::complex<double>>& _dispvec);

  template<typename ContainerType>
  void compute(unsigned nblocks, ContainerType& out);

  void compute(unsigned nblocks, std::complex<T>* out);

protected:
  unsigned npol;
  double period;

  unsigned len_fft;
  unsigned n_disp_low;
  unsigned n_disp_high;
  unsigned nclip;

  unsigned phase_bins;

  double sampling_rate;

  std::vector<std::vector<std::complex<T>>> coherency;
  std::vector<std::complex<double>> dispvec;

private:

  std::default_random_engine generator;
  std::normal_distribution<T> distribution;

};
}


#endif
