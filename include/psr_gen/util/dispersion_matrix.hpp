#ifndef __dispersion_matrix_hpp
#define __dispersion_matrix_hpp

#include <math.h>
#include <tuple>
#include <vector>
#include <complex>
#include <iostream>
#include <algorithm>

#include "psr_gen/config.hpp"
#include "psr_gen/util/util.hpp"

namespace psr_gen {
namespace util {


template<typename dtype>
class DispersionMatrix {

const double D_const = 4.148804e3; //  s.MHz^2/(pc/cm^3)

public:
  DispersionMatrix (
    const two_tuple& _freq_range,
    const double& _DM,
    const double& _usable_bandwidth = 1.0
  );

  ~DispersionMatrix ();

  void compute(
    std::vector<std::complex<dtype>>& result,
    int& n_disp_low,
    int& n_disp_high,
    unsigned len_fft,
    unsigned nchan,
    double sampling_period,
    int direction = 1
  );

  two_tuple get_freq_range() const {
    return freq_range; }
  void set_freq_range (const two_tuple& _freq_range ) {
    freq_range = _freq_range; }

  double get_DM () const {
    return DM; }
  void set_DM (const double& _DM) {
    DM = _DM; }

  double get_usable_bandwidth () const {
    return usable_bandwidth; }
  void set_usable_bandwidth (const double& _usable_bandwidth) {
    usable_bandwidth = _usable_bandwidth; }

  double get_bandwidth () const {
    return std::get<1>(freq_range) - std::get<0>(freq_range);}

private:

  two_tuple freq_range;
  double DM;
  double usable_bandwidth;

};

template<typename dtype>
DispersionMatrix<dtype>::DispersionMatrix(
  const two_tuple& _freq_range,
  const double& _DM,
  const double& _usable_bandwidth
) : freq_range(_freq_range), DM(_DM), usable_bandwidth(_usable_bandwidth)
{
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::util::DispersionMatrix::DispersionMatrix: freq_range=["
      << std::get<0>(freq_range) << ", " << std::get<1>(freq_range) << "]" << std::endl;
  }

}

template<typename dtype>
DispersionMatrix<dtype>::~DispersionMatrix () {}


/**
 * [compute description]
 * @method compute
 * @param  result          [description]
 * @param  n_disp_low      number of trailing elements that are removed
 * @param  n_disp_high     number of leading elements that are removed
 * @param  len_fft         [description]
 * @param  nchan           [description]
 * @param  sampling_period [description]
 * @param  direction       [description]
 */
template<typename dtype>
void DispersionMatrix<dtype>::compute(
  std::vector<std::complex<dtype>>& result,
  int& n_disp_low,
  int& n_disp_high,
  unsigned len_fft,
  unsigned nchan,
  double sampling_period,
  int direction
)
{

  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::util::DispersionMatrix::compute: len_fft=" << len_fft << std::endl;
    std::cerr << "psr_gen::util::DispersionMatrix::compute: nchan=" << nchan << std::endl;
    std::cerr << "psr_gen::util::DispersionMatrix::compute: sampling_period=" << sampling_period << std::endl;
    std::cerr << "psr_gen::util::DispersionMatrix::compute: direction=" << direction << std::endl;
  }

  double DM_D_const = DM * D_const;

  double f0 = std::get<0>(freq_range);

  double bandwidth = get_bandwidth();

  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::util::DispersionMatrix::compute: bandwidth=" << bandwidth << std::endl;
    std::cerr << "psr_gen::util::DispersionMatrix::compute: DM_D_const=" << DM_D_const << std::endl;
  }


  double delta_freq = bandwidth / ((double) (nchan * len_fft));

  std::vector<dtype> freqs(nchan * len_fft);
  std::vector<dtype> freqs_mean (nchan);
  if (result.size() != nchan * len_fft) {
    result.resize(nchan * len_fft);
  }
  std::complex<dtype> z_direction (0, 2.0*M_PI*direction);


  unsigned idx;
  std::complex<dtype> z_phi;
  for (unsigned ichan=0; ichan<nchan; ichan++)
  {
    for (unsigned ifft=0; ifft<len_fft; ifft++)
    {
      idx = ichan * len_fft + ifft;
      freqs[idx] = f0 + delta_freq * idx;
      freqs_mean[ichan] += freqs[idx];
    }
    freqs_mean[ichan] /= len_fft;

    for (unsigned ifft=0; ifft<len_fft; ifft++)
    {
      idx = ichan * len_fft + ifft;
      z_phi.real(DM_D_const*pow(freqs[idx]-freqs_mean[ichan], 2)/pow(freqs_mean[ichan], 2)/freqs[idx]*1E6);
      result[idx] = exp(z_direction * z_phi);
    }
  }
  // usable_bandwidth parameter


  // calculate n_disp_low, and n_disp_high

  std::vector<dtype> freq_chan0_range = {freqs[0], freqs[len_fft-1]};
  dtype min_freq_chan0 = std::min(freq_chan0_range[0], freq_chan0_range[1]);
  dtype max_freq_chan0 = std::max(freq_chan0_range[0], freq_chan0_range[1]);
  dtype freq_chan0_mean = (freq_chan0_range[1] + freq_chan0_range[0]) / 2.0;

  n_disp_high = (int) std::ceil(
    DM_D_const*(1.0/std::pow(freq_chan0_mean, 2) -
                1.0/std::pow(max_freq_chan0, 2))/sampling_period);
  n_disp_low = (int) std::ceil(
    DM_D_const*(1.0/std::pow(min_freq_chan0, 2) -
                1.0/std::pow(freq_chan0_mean, 2))/sampling_period);
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::util::DispersionMatrix::compute: n_disp_low=" << n_disp_low << std::endl;
    std::cerr << "psr_gen::util::DispersionMatrix::compute: n_disp_high=" << n_disp_high << std::endl;
  }

}

}
}





#endif
