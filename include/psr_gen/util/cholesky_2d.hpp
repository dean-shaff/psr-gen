#ifndef __cholesky_2d_hpp
#define __cholesky_2d_hpp

#include <string>
#include <cmath>

#include "util.hpp"

namespace psr_gen {
namespace util {

/**
 * Compute the cholesky decomposition of a 2x2 matrix.
 * @method cholesky_2d
 * @param  src         [description]
 */
template<typename T>
void cholesky_2d (T* src, const std::string& side, bool conj=false) {
  src[0] = std::sqrt(src[0]);
  // src[2] = (psr_gen::util::std_type_map<T>::init(1.0)/src[0]) * src[2];
  src[2] = src[2] / src[0];
  src[3] = std::sqrt(src[3] - (src[2] * psr_gen::util::std_type_map<T>::conj(src[2])));

  if (side == "lower" || side == "l") {
    src[1] = psr_gen::util::std_type_map<T>::init(0.0);
  } else if (side == "upper" || side == "u") {
    if (conj) {
      src[1] = psr_gen::util::std_type_map<T>::conj(src[2]);
    } else {
      src[1] = src[2];
    }
    src[2] = psr_gen::util::std_type_map<T>::init(0.0);
  }

}


}
}


#endif
