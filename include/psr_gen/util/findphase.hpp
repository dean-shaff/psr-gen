#ifndef __findphase_hpp
#define __findphase_hpp

#include <vector>
#include <math.h>

namespace psr_gen {
namespace util {



/**
 * Given some time, find the associated pulsar phase bin
 * 
 * Original Matlab code:
 * f = mod(t, pcal.a)/pcal.a;
 * phase = transpose(round(f*nbins+0.5));
 *
 * @method findphase
 * @param  time          time in seconds
 * @param  phase_bins    number of pulsar phase bins
 * @param  pulsar_period period of pulsar
 * @return pulsar phase bin
 */
template<typename T>
T findphase (
  const T time,
  unsigned phase_bins,
  double pulsar_period
)
{
  T phase;
  if (time < 0.0) {
    phase = (std::fmod(pulsar_period + time, pulsar_period)) / pulsar_period;
  } else {
    phase = (std::fmod(time, pulsar_period)) / pulsar_period;
  }
  phase = std::round(phase*(T)phase_bins + 0.5) - 1; // take into account C++ indexing
  if (phase == phase_bins) {
    phase -= 1;
  }
  return phase;
}


/**
 * [findphase  description]
 * @method findphase
 * @param  times         vector of times, in seconds
 * @param  phase_bins    number of pulsar phase bins
 * @param  pulsar_period period of pulsar
 * @param  phases        results vector
 */
template<typename T>
void findphase (
  const std::vector<T>& times,
  unsigned phase_bins,
  double pulsar_period,
  std::vector<T>& phases
)
{

  if (phases.size() != times.size()) {
    phases.resize(times.size());
  }
  for (unsigned idx=0; idx<phases.size(); idx++) {
    phases[idx] = psr_gen::util::findphase<T>(times[idx], phase_bins, pulsar_period);
  }

}


}
}



#endif
