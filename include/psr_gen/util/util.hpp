#ifndef __util_hpp
#define __util_hpp

#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <initializer_list>
#include <complex>
#include <exception>
#include <chrono>

#include <fftw3.h>


namespace psr_gen {
namespace util {

typedef std::tuple<double, double> two_tuple;

template<typename T>
struct std_type_map;

template<>
struct std_type_map<float> {
  static float init (double val) {
    return (float) val;
  }

  static float conj (float val) {
    return val;
  }

};

template<>
struct std_type_map<double> {
  static double init (double val) {
    return val;
  }
  static double conj (double val) {
    return val;
  }
};

template<>
struct std_type_map<std::complex<float>> {
  static std::complex<float> init (double val) {
    return std::complex<float>((float) val, 0.0);
  }

  static std::complex<float> conj (std::complex<float> val) {
    return std::conj(val);
  }
};

template<>
struct std_type_map<std::complex<double>> {
  static std::complex<double> init (double val) {
    return std::complex<double>(val, 0.0);
  }

  static std::complex<double> conj (std::complex<double> val) {
    return std::conj(val);
  }
};



template<typename unit>
using delta = std::chrono::duration<double, unit>;

typedef std::chrono::time_point<std::chrono::high_resolution_clock> time_point;

inline time_point now () {
 return std::chrono::high_resolution_clock::now();
}


template<typename T>
struct fftw3_type_map;

template<>
struct fftw3_type_map<std::complex<float>>
{
  typedef fftwf_complex type;
  typedef fftwf_plan plan_type;

  template<class ... Types>
  static plan_type make_plan_many (Types ... args) {
      return fftwf_plan_many_dft(args...);
  }

  static void execute_plan (plan_type plan) {
    fftwf_execute(plan);
  };

  static void destroy_plan (plan_type plan) {
    fftwf_destroy_plan(plan);
  };
};

template<>
struct fftw3_type_map<std::complex<double>>
{
  typedef fftw_complex type;
  typedef fftw_plan plan_type;

  template<class ... Types>
  static plan_type make_plan_many (Types ... args) {
      return fftw_plan_many_dft(args...);
  }

  static void execute_plan (plan_type plan) {
    fftw_execute(plan);
  };

  static void destroy_plan (plan_type plan) {
    fftw_destroy_plan(plan);
  };
};


template<typename T>
void unique_sort(
  const std::vector<T>& src,
  std::vector<T>& dst
)
{
  dst = std::vector<T>(src.begin(), src.end());
  std::sort(dst.begin(), dst.end());
  auto unique_it = std::unique(dst.begin(), dst.end());
  dst.resize(std::distance(dst.begin(), unique_it));
}

template<typename T, typename FuncType>
void where (
  const std::vector<T>& src,
  std::vector<bool>& dst,
  unsigned& ntrue,
  FuncType comparison
)
{
  ntrue = 0;
  if (dst.size() != src.size()) {
    dst.resize(src.size());
  }
  for (unsigned idx=0; idx<src.size(); idx++) {
    if (comparison(src[idx])) {
      dst[idx] = true;
      ntrue++;
    } else {
      dst[idx] = false;
    }
  }
}

template<typename T, typename FuncType>
void where (
  const std::vector<T>& src,
  std::vector<bool>& dst,
  FuncType comparison
)
{
  unsigned ntrue;
  psr_gen::util::where<T, FuncType>(src, dst, ntrue, comparison);
}

template<typename T>
bool isnan(
  std::complex<T> val
)
{
  return std::isnan(val.real()) || std::isnan(val.imag());
}


}
}



#endif
