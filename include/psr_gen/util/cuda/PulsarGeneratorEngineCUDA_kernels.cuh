#ifndef __PulsarGeneratorEngineCUDA_kernels_cuh
#define __PulsarGeneratorEngineCUDA_kernels_cuh

#include <type_traits>
#include <stdio.h>

#include "util.cuh"

#ifndef TEST_MODE
#define TEST_MODE 0
#endif

namespace psr_gen {
namespace util {
namespace cuda {

template<typename T>
__device__ T findphase (
  const T time,
  unsigned phase_bins,
  double pulsar_period
)
{
  typedef psr_gen::util::cuda::cufft_type_map<T> type_map;
  T phase;
  if (time < 0.0) {
    phase = (type_map::fmod(pulsar_period + time, pulsar_period)) / pulsar_period;
  } else {
    phase = (type_map::fmod(time, pulsar_period)) / pulsar_period;
  }
  phase = type_map::round(phase*(T)phase_bins + 0.5) - 1; // take into account C++ indexing
  if (phase == phase_bins) {
    phase -= 1;
  }
  return phase;
}

template<typename T>
__device__ void cholesky_2d(
  T* arr,
  const char side,
  bool conj=false
)
{
  typedef psr_gen::util::cuda::cufft_type_map<T> type_map;

  arr[0] = type_map::sqrt(arr[0]);
  // arr[2] = type_map::mul(type_map::div(type_map::init(1.0), arr[0]), arr[2]);
  arr[2] = type_map::div(arr[2], arr[0]);
  arr[3] = type_map::sqrt(
    type_map::sub(arr[3], type_map::mul(arr[2], type_map::conj(arr[2]))));

  if (side == 'l') {
    arr[1] = type_map::init(0.0);
  } else if (side == 'u') {
    if (conj) {
      arr[1] = type_map::conj(arr[2]); // this should be needed
    } else {
      arr[1] = arr[2];
    }
    arr[2] = type_map::init(0.0);
  }
}

__global__ void init_rand (
  curandState* states,
  unsigned max_offset,
  unsigned seed
);


template<typename T>
__global__ void sift_phases (
  const T* indices,
  unsigned len_indices,
  const T* phases,
  typename cufft_type_map<T>::complex_type* pulsar_vec,
  typename cufft_type_map<T>::complex_type* coherency,
  unsigned npol,
  unsigned len_fft, // this is the size of the phases array
  curandState* rand_states
)
{

  if (threadIdx.x > len_fft || blockIdx.x > len_indices) {
    return;
  }

  typedef typename cufft_type_map<T>::complex_type complex_type;
  typedef typename cufft_type_map<T>::type float_type;

  extern __shared__ unsigned char _scratch[];
  complex_type* scratch = reinterpret_cast<complex_type*>(_scratch);

  const unsigned total_size_idx = gridDim.x;
  const unsigned total_size_iphase = blockDim.x;

  unsigned index;

  const float_type sqrt_half = cufft_type_map<T>::sqrt(0.5);

  complex_type random_phasors[2];

  curandState rand_state = rand_states[threadIdx.x];

  for (unsigned idx=blockIdx.x; idx<len_indices; idx+=total_size_idx) {
    index = (unsigned) indices[idx];
    if (threadIdx.x == 0) {
      scratch[0] = coherency[index];
      scratch[1] = coherency[len_fft + index];
      scratch[2] = coherency[2*len_fft + index];
      scratch[3] = coherency[3*len_fft + index];
      psr_gen::util::cuda::cholesky_2d(scratch, 'u');
    }
    __syncthreads();

    for (unsigned iphase=threadIdx.x; iphase<len_fft; iphase+=total_size_iphase) {

      if (phases[iphase] == indices[idx]) {

        for (unsigned ipol=0; ipol<npol; ipol++) {
        #if TEST_MODE == 1
          random_phasors[ipol].x = (float_type) iphase;
          random_phasors[ipol].y = (float_type) iphase;
        #else
          random_phasors[ipol].x = sqrt_half*cufft_type_map<float_type>::normal(&rand_state);
          random_phasors[ipol].y = sqrt_half*cufft_type_map<float_type>::normal(&rand_state);
          // random_phasors[ipol].x = cufft_type_map<float_type>::normal(&rand_state);
          // random_phasors[ipol].y = cufft_type_map<float_type>::normal(&rand_state);
        #endif
        }
        for (unsigned ipol=0; ipol<npol; ipol++) {
          pulsar_vec[npol*iphase + ipol] = cufft_type_map<complex_type>::add(
            cufft_type_map<complex_type>::mul(random_phasors[0], scratch[ipol]),
            cufft_type_map<complex_type>::mul(random_phasors[1], scratch[npol + ipol])
          );
          // pulsar_vec[npol*iphase + ipol] = random_phasors[ipol];
        }
      }
    }
    rand_states[threadIdx.x] = rand_state;
  }
}

template<typename T>
__global__ void multiply_dispersion_kernel (
  typename cufft_type_map<T>::complex_type* pulsar_vec,
  typename cufft_type_map<T>::complex_type* dispersion,
  unsigned npol,
  unsigned len_fft // this is the size of the phases array
)
{
  typedef typename cufft_type_map<T>::complex_type complex_type;
  const unsigned total_size_idx = blockDim.x*gridDim.x;
  const unsigned total_size_ipol = gridDim.y;

  for (unsigned idx=(blockIdx.x*blockDim.x + threadIdx.x); idx<len_fft; idx+=total_size_idx) {
    for (unsigned ipol=blockIdx.y; ipol<npol; ipol+=total_size_ipol) {
      pulsar_vec[npol*idx + ipol] = cufft_type_map<complex_type>::mul(
        pulsar_vec[npol*idx + ipol],
        dispersion[idx]);
    }
  }
}

template<typename T>
__global__ void save_overlap (
  typename cufft_type_map<T>::complex_type* pulsar_vec,
  typename cufft_type_map<T>::complex_type* out,
  typename cufft_type_map<T>::complex_type scalar,
  unsigned npol,
  unsigned nclip,
  unsigned n_disp_high
)
{
  typedef typename cufft_type_map<T>::complex_type complex_type;

  const unsigned total_size_idx = blockDim.x*gridDim.x;
  const unsigned total_size_ipol = gridDim.y;

  for (unsigned idx=(blockIdx.x*blockDim.x + threadIdx.x); idx<nclip; idx+=total_size_idx) {
    for (unsigned ipol=blockIdx.y; ipol<npol; ipol+=total_size_ipol) {
      out[idx*npol + ipol] = cufft_type_map<complex_type>::mul(
        scalar, pulsar_vec[npol*(idx+n_disp_high) + ipol]);
    }
  }
}

template<typename T>
__global__ void populate_phase (
  T* phase_vec,
  unsigned len_fft,
  double sampling_rate,
  unsigned phase_bins,
  double period,
  unsigned n_disp_high,
  unsigned iblock
)
{
  const unsigned total_size_idx = blockDim.x*gridDim.x;
  const unsigned nclip_high = len_fft - 2*n_disp_high;
  T tval;
  for (unsigned idx=(blockIdx.x*blockDim.x + threadIdx.x); idx<len_fft; idx+=total_size_idx) {
    if (iblock == 0) {
      tval = ((T) idx - n_disp_high)*sampling_rate;
    } else {
      tval = ((T) (iblock*(nclip_high) + idx - n_disp_high)) * sampling_rate;
    }
    phase_vec[idx] = psr_gen::util::cuda::findphase(tval, phase_bins, period);
  }
}



}
}
}


#endif
