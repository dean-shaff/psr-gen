// sqrt implementation for complex number taken from https://devtalk.nvidia.com/default/topic/814159/additional-cucomplex-functions-cucnorm-cucsqrt-cucexp-and-some-complex-double-functions-/
#ifndef __cuda_util_hpp
#define __cuda_util_hpp

#include <complex>

#include <cuda_runtime.h>
#include <cufft.h>
#include <curand_kernel.h>


namespace psr_gen {
namespace util {
namespace cuda {

  template<typename Type>
  struct is_complex {
    static constexpr bool value = false;
  };

  template<>
  struct is_complex<cufftComplex> {
    static constexpr bool value = true;
  };

  template<>
  struct is_complex<cufftDoubleComplex> {
    static constexpr bool value = true;
  };

  template<typename T, typename Enable=void>
  struct base_cufft_type_map;

  template<typename Type>
  struct base_cufft_type_map<
    Type, typename std::enable_if<!is_complex<Type>::value>::type
  >  {
    __host__ __device__ static inline Type mul (Type a, Type b) {
      return a*b;
    }
    __host__ __device__ static inline Type div (Type a, Type b) {
      return a/b;
    }
    __host__ __device__ static inline Type add (Type a, Type b) {
      return a + b;
    }
    __host__ __device__ static inline Type sub (Type a, Type b) {
      return a - b;
    }
    __host__ __device__ static inline Type init (double a) {
      return (Type) a;
    }
    __host__ __device__ static inline Type conj (Type a) {
      return a;
    }
  };

  template<typename Type>
  struct cufft_type_map;

  template<>
  struct cufft_type_map<float> : base_cufft_type_map<float> {
    typedef cufftComplex complex_type;
    typedef float type;

    __host__ __device__ static inline type sqrt (float val) {
      return sqrtf(val);
    }

    __host__ __device__ static inline type fmod (float val0, float val1) {
      return fmodf(val0, val1);
    }

    __host__ __device__ static inline type round (float val) {
      return roundf(val);
    }

    __device__ static inline type normal (curandState* state) {
      return curand_normal(state);
    }

  };

  template<>
  struct cufft_type_map<double> : base_cufft_type_map<double> {
    typedef cufftDoubleComplex complex_type;
    typedef double type;

    __host__ __device__ static inline type sqrt (double val) {
      return sqrt(val);
    }

    __host__ __device__ static inline type fmod (double val0, double val1) {
      return fmod(val0, val1);
    }

    __host__ __device__ static inline type round (double val) {
      return round(val);
    }

    __device__ static inline type normal (curandState* state) {
      return curand_normal_double(state);
    }


  };

  template<>
  struct cufft_type_map<cufftComplex> {
    typedef cufftComplex complex_type;
    typedef float type;

    __host__ __device__ static inline complex_type mul (complex_type a, complex_type b) {
      return cuCmulf(a, b);
    }
    __host__ __device__ static inline complex_type div (complex_type a, complex_type b) {
      return cuCdivf(a, b);
    }
    __host__ __device__ static inline complex_type add (complex_type a, complex_type b) {
      return cuCaddf(a, b);
    }
    __host__ __device__ static inline complex_type sub (complex_type a, complex_type b) {
      return cuCsubf(a, b);
    }
    __host__ __device__ static inline complex_type init (double a) {
      return make_cuComplex((type) a, 0.0);
    }
    __host__ __device__ static inline complex_type conj (complex_type a) {
      return cuConjf(a);
    }
    __host__ __device__ static inline complex_type sqrt (complex_type val) {
      float radius = cuCabsf(val);
    	float cosA = val.x / radius;
    	complex_type out;
    	out.x = sqrtf(radius * (cosA + 1.0) / 2.0);
    	out.y = sqrtf(radius * (1.0 - cosA) / 2.0);
    	// signbit should be false if val.y is negative
    	if (signbit(val.y))
    		out.y *= -1.0;
    	return out;
    }

    static const cufftType flag = CUFFT_C2C;

    template<class ... Types>
    static cufftResult execute (Types ... args)
    {
      return cufftExecC2C(args...);
    }



  };

  template<>
  struct cufft_type_map<cufftDoubleComplex> {
    typedef cufftDoubleComplex complex_type;
    typedef double type;

    __host__ __device__ static inline complex_type mul (complex_type a, complex_type b) {
      return cuCmul(a, b);
    }
    __host__ __device__ static inline complex_type div (complex_type a, complex_type b) {
      return cuCdiv(a, b);
    }
    __host__ __device__ static inline complex_type add (complex_type a, complex_type b) {
      return cuCadd(a, b);
    }
    __host__ __device__ static inline complex_type sub (complex_type a, complex_type b) {
      return cuCsub(a, b);
    }
    __host__ __device__ static inline complex_type init (double a) {
      return make_cuDoubleComplex((type) a, 0.0);
    }
    __host__ __device__ static inline complex_type conj (complex_type a) {
      return cuConj(a);
    }

    __host__ __device__ static inline complex_type sqrt (complex_type val) {
      double radius = cuCabs(val);
    	double cosA = val.x / radius;
    	complex_type out;
      // need the following so it doesn't call this function recursively.
    	out.x = cufft_type_map<type>::sqrt(radius * (cosA + 1.0) / 2.0);
    	out.y = cufft_type_map<type>::sqrt(radius * (1.0 - cosA) / 2.0);
    	// signbit should be false if val.y is negative
    	if (signbit(val.y)) {
    		out.y *= -1.0;
      }

    	return out;
    }



    static const cufftType flag = CUFFT_Z2Z;

    template<class ... Types>
    static cufftResult execute (Types ... args)
    {
      return cufftExecZ2Z(args...);
    }


  };



}
}
}



#endif
