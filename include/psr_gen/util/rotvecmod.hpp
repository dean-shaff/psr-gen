#ifndef __rotvecmod_hpp
#define __rotvecmod_hpp

#include <math.h>
#include <vector>
#include <complex>

#include "psr_gen/config.hpp"

namespace psr_gen {
namespace util {

template<typename T>
T rad2deg (T rad)
{
  return rad * 180.0 / M_PI;
}

template<typename T>
T deg2rad (T deg)
{
  return deg * M_PI / 180.0;
}


/**
 * Rotating vector model for pulsar emission.
 * Translation into C++ of Matlab code.
 * @method rotvecmod
 * @param  size      [description]
 * @param  noise     [description]
 * @param  stokes    [description]
 * @param  coherency [description]
 * @param  phase     [description]
 */
template<typename T>
void rotvecmod (
  unsigned size,
  double noise,
  std::vector<std::vector<T>>& stokes,
  std::vector<std::vector<std::complex<T>>>& coherency,
  std::vector<T>& phases
)
{
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::util::rotvecmod: size=" << size << std::endl;
    std::cerr << "psr_gen::util::rotvecmod: noise=" << noise << std::endl;    
  }

  const unsigned nstokes_params = 4;

  const double esig = 5.0; // emission half angle (polar angle, degrees)
  const double epeak = 0.0 ; // emission peak angle (polar angle, degrees)
  const double flin = 0.3; // linear polarized fraction amplitude

  const double zeta = deg2rad<double>(30.0); // observing angle (degrees) relative to rotation axis
  const double sin_zeta = std::sin(zeta);
  const double cos_zeta = std::cos(zeta);

  const double alpha = deg2rad<double>(40.0); // magnetic axis (degrees) relative to rotation axis
  const double sin_alpha = std::sin(alpha);
  const double cos_alpha = std::cos(alpha);

  const double pmin = -M_PI;
  const double pmax = M_PI;
  const double pstep = (pmax - pmin) / (size - 1);

  if (stokes.size() < nstokes_params) {
    stokes.resize(nstokes_params);
  }

  if (coherency.size() < nstokes_params) {
    coherency.resize(nstokes_params);
  }

  phases.resize(size);
  for (unsigned idx=0; idx<stokes.size(); idx++) {
    stokes[idx].resize(size);
    coherency[idx].resize(size);
  }

  T psi;
  T cosO;
  T tanO;
  T thetaE;
  T thetaE_deg;
  T max_S0_0;
  T max_S0_1;

  std::vector<T> psis(size);
  std::vector<T> thetaEs(size);



  for (unsigned idx=0; idx<size; idx++)
  {
    phases[idx] = idx*pstep + pmin;

    psi = std::atan2(
      sin_alpha*std::sin(phases[idx]),
      sin_zeta*cos_alpha - sin_alpha*cos_zeta*std::cos(phases[idx])
    );
    // std::cerr << "p=" << phases[idx] << " p (deg) =" << rad2deg<double>(phases[idx]) << std::endl;
    // std::cerr << "psi=" << psi << " psi (deg)=" << rad2deg<double>(psi) << std::endl;
    psis[idx] = psi;

    cosO = std::cos(phases[idx])*sin_zeta*sin_alpha + cos_alpha*cos_zeta;
    tanO = std::sqrt(1.0/std::pow(cosO, 2) - 1.0);
    thetaE = atan(
      1.5 * (std::sqrt(1 + (8.0/9.0)*std::pow(tanO, 2)) - 1) / tanO
    );
    thetaEs[idx] = thetaE;
    thetaE_deg = rad2deg<T>(thetaE);


    stokes[0][idx] = (1./std::sqrt(2*M_PI*esig*esig))*
                     std::exp(-std::pow((thetaE_deg - epeak), 2)/(2.0*esig*esig));

    if (idx == 0) {
      max_S0_0 = stokes[0][idx];
    }

    if (stokes[0][idx] > max_S0_0) {
      max_S0_0 = stokes[0][idx];
    }

  }

  T lin_frac;

  for (unsigned idx=0; idx<size; idx++) {
    stokes[0][idx] /= max_S0_0;

    lin_frac = flin*stokes[0][idx]*std::cos(thetaEs[idx]);

    stokes[1][idx] = lin_frac*std::cos(2.0*psis[idx]);
    stokes[2][idx] = lin_frac*std::sin(2.0*psis[idx]);
    stokes[3][idx] = -(1.0-flin)*stokes[1][idx];

    stokes[0][idx] += noise;

    if (idx == 0) {
      max_S0_1 = stokes[0][idx];
    }

    if (stokes[0][idx] > max_S0_1) {
      max_S0_1 = stokes[0][idx];
    }

  }

  for (unsigned idx=0; idx<size; idx++) {
    phases[idx] = rad2deg<T>(phases[idx]);
    stokes[0][idx] /= max_S0_1;
    stokes[1][idx] /= max_S0_1;
    stokes[2][idx] /= max_S0_1;
    stokes[3][idx] /= max_S0_1;
    // Jxx
    coherency[0][idx] = std::complex<T>(
      0.5*(stokes[0][idx] + stokes[1][idx]), 0.0);
    // Jyy
    coherency[3][idx] = std::complex<T>(
      0.5*(stokes[0][idx] - stokes[1][idx]), 0.0);
    // Jyx
    coherency[1][idx] = std::complex<T>(
      0.5*stokes[2][idx], -0.5*stokes[3][idx]);
    // Jxy
    coherency[2][idx] = std::complex<T>(
      0.5*stokes[2][idx], 0.5*stokes[3][idx]);

    // std::cerr << "last loop idx=" << idx << " coherency="
    // << coherency[0][idx] <<  ", "
    // << coherency[1][idx] <<  ", "
    // << coherency[2][idx] <<  ", "
    // << coherency[3][idx]
    //   << std::endl;
  }

  // std::cerr << "psr_gen::util::rotvecmod" << std::endl;


}


}
}



#endif
