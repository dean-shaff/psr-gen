#ifndef __PulsarGenerator_hpp
#define __PulsarGenerator_hpp

#include <iostream>
#include <sstream>
#include <complex>
#include <vector>
#include <algorithm>
#include <exception>
#include <chrono>

#include "util/dispersion_matrix.hpp"
#include "util/rotvecmod.hpp"
#include "util/util.hpp"
#include "PulsarGeneratorEngineOpenMP.hpp"

namespace psr_gen {

  // template<typename T, typename EngineType>
template<typename T, typename EngineType=PulsarGeneratorEngineOpenMP<T>>
class PulsarGenerator {

public:
  /**
   * @param bandwidth The size of the bandwidth in MHz
   * @param centre_freq The centre frequency of the pulsar signal
   * @param npol The number of polarizations to generate, either one or two
   * @param DM The dispersion measure of the pulsar, in pc/cm**3
   * @param period The period the pulsar, in seconds
   * @param noise Amount of noise to inject in pulsar signal. 0 is no noise,
   *   1.0 is all the noise
   */
  PulsarGenerator(
    double _bandwidth,
    double _centre_freq,
    unsigned _npol,
    double _DM,
    double _period,
    double _noise,
    unsigned _len_fft,
    unsigned _phase_bins);

  ~PulsarGenerator ();

  template<typename ContainerType>
  void compute(double time, ContainerType& out);

  void compute(double time, std::complex<T>* out);

  unsigned samples_for (double time);

  unsigned blocks_for (double time);

  double get_bandwidth() const { return bandwidth; }

  double get_centre_freq() const { return centre_freq; }

  unsigned get_npol() const { return npol; }

  double get_DM() const { return DM; }

  double get_period() const { return period; }

  double get_noise() const { return noise; }

  unsigned get_len_fft() const { return len_fft; }

  unsigned get_phase_bins() const { return phase_bins; }

  double get_sampling_rate() const { return sampling_rate; }

  unsigned get_nclip() const { return nclip; }

  int get_n_disp_low() const { return n_disp_low; }

  int get_n_disp_high() const { return n_disp_high; }

  util::two_tuple get_freq_range() const { return freq_range; }


private:

  double bandwidth;
  double centre_freq;
  unsigned npol;
  double DM;
  double period;
  double noise;
  unsigned len_fft;
  unsigned phase_bins;

  // computed properties
  double sampling_rate;
  unsigned nclip;
  int n_disp_low;
  int n_disp_high;

  util::two_tuple freq_range;

  util::DispersionMatrix<double>* dispmatrix;
  std::vector<std::complex<double>> dispvec;

  std::vector<std::vector<T>> stokes;
  std::vector<std::vector<std::complex<T>>> coherency;
  std::vector<T> phases;

  EngineType* engine;

};

}

template<typename T, typename EngineType>
psr_gen::PulsarGenerator<T, EngineType>::~PulsarGenerator () {
  delete dispmatrix;
  delete engine;
}


template<typename T, typename EngineType>
psr_gen::PulsarGenerator<T, EngineType>::PulsarGenerator (
  double _bandwidth,
  double _centre_freq,
  unsigned _npol,
  double _DM,
  double _period,
  double _noise,
  unsigned _len_fft,
  unsigned _phase_bins
) : bandwidth(_bandwidth), centre_freq(_centre_freq), npol(_npol), DM(_DM),
    period(_period), noise(_noise), len_fft(_len_fft), phase_bins(_phase_bins)
{
  if (config::verbose()) {
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: bandwidth=" << bandwidth << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: centre_freq=" << centre_freq << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: npol=" << npol << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: DM=" << DM << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: period=" << period << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: noise=" << noise << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: len_fft=" << len_fft << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: phase_bins=" << phase_bins << std::endl;
  }

  std::get<0>(freq_range) = -bandwidth/2.0 + centre_freq;
  std::get<1>(freq_range) = bandwidth/2.0 + centre_freq;

  dispmatrix = new util::DispersionMatrix<double> (
    freq_range, DM);

  unsigned nchan = 1;
  sampling_rate = (1.0/bandwidth)*1e-6;

  if (config::verbose()) {
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: nchan=" << nchan << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: sampling_rate=" << sampling_rate << std::endl;
  }

  dispmatrix->compute(
    dispvec,
    n_disp_low,
    n_disp_high,
    len_fft,
    nchan,
    sampling_rate);

  if ((unsigned)(n_disp_low + n_disp_high) >= len_fft) {
    std::stringstream err_msg;
    err_msg << "psr_gen::PulsarGenerator::PulsarGenerator: "
      << "Can't construct dispersion kernel with DM=" << DM
      << " and frequency range=[" << std::get<0>(freq_range) << ", "
      << std::get<1>(freq_range) << "] and len_fft=" << len_fft;
    throw std::runtime_error(err_msg.str());
  }

  nclip = len_fft - n_disp_low - n_disp_high;

  if (config::verbose()) {
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: n_disp_low=" << n_disp_low << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: n_disp_high=" << n_disp_high << std::endl;
    std::cerr << "psr_gen::PulsarGenerator::PulsarGenerator: nclip=" << nclip << std::endl;
  }

  stokes = std::vector<std::vector<T>>(4);
  coherency = std::vector<std::vector<std::complex<T>>>(4);

  util::rotvecmod<T>(
    phase_bins,
    noise,
    stokes,
    coherency,
    phases);

  engine = new EngineType(
    npol,
    period,
    len_fft,
    n_disp_low,
    n_disp_high,
    phase_bins,
    sampling_rate,
    coherency,
    dispvec);
}

template<typename T, typename EngineType>
template<typename ContainerType>
void psr_gen::PulsarGenerator<T, EngineType>::compute (double time, ContainerType& out)
{

  unsigned nblocks = (unsigned) (time / (nclip * sampling_rate));
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::PulsarGeneratorEngine::compute: nblocks=" << nblocks << std::endl;
  }
  if (nblocks == 0) {
    std::stringstream err_msg;
    err_msg << "psr_gen::PulsarGenerator::compute: "
      << "len_fft=" << len_fft
      << " is too long for time=" << time
      << "; try decreasing len_fft";
    throw std::runtime_error(err_msg.str());
  }
  out.resize(nblocks*nclip*npol);
  engine->template compute<ContainerType>(nblocks, out);
}

template<typename T, typename EngineType>
void psr_gen::PulsarGenerator<T, EngineType>::compute (double time, std::complex<T>* out)
{

  unsigned nblocks = blocks_for(time);
  if (psr_gen::config::verbose()) {
    std::cerr << "psr_gen::PulsarGeneratorEngine::compute: nblocks=" << nblocks << std::endl;
  }
  if (nblocks == 0) {
    std::stringstream err_msg;
    err_msg << "psr_gen::PulsarGenerator::compute: "
      << "len_fft=" << len_fft
      << " is too long for time=" << time
      << "; try decreasing len_fft";
    throw std::runtime_error(err_msg.str());
  }
  engine->compute(nblocks, out);
}

template<typename T, typename EngineType>
unsigned psr_gen::PulsarGenerator<T, EngineType>::samples_for (double time)
{
  unsigned nblocks = blocks_for(time);
  return nblocks*nclip*npol;
}

template<typename T, typename EngineType>
unsigned psr_gen::PulsarGenerator<T, EngineType>::blocks_for (double time)
{
  unsigned nblocks = (unsigned) (time / (nclip * sampling_rate));
  return nblocks;
}



#endif
