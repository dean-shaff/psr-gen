#ifndef __PulsarGeneratorEngine_hpp
#define __PulsarGeneratorEngine_hpp

namespace psr_gen {

  template<typename Type>
  class PulsarGeneratorEngine {

  public:

    template<class ... Types>
    PulsarGeneratorEngine (Types ... args) {}

    template<class ... Types>
    void compute (Types ... args) {}

  };

}

#endif
