psr-gen: A Pulsar Signal Generator
==================================

C++11/OpenMP pulsar generator.

Part of the PST Golden Signal Chain Model is a pulsar signal generator. This code is used heavily for creating idealized simulated pulsar data, which can be fed through a variety of pulsar timing signal pipelines. This code was originally written in Matlab, and can be found `here <https://github.com/SKA-PST/PST_Matlab_pulsar_signal_processing_model_CDR>`_.

While this code has proven incredibly useful, it is not portable nor performant, given that it is written in Matlab. ``psr-gen`` is C++11 library that uses OpenMP and CUDA to rapidly produce simulated pulsar data.


Building
--------

This project requires OpenMP and both single and double precision builds of FFTW. If you installed FFTW at the system level, CMake should be able to find it. Otherwise, you can use the ``FFTW_DIR`` CMake flag to indicate the directory in which FFTW header files and built libraries are installed.

With FFTW and OpenMP installed:

.. code-block::


  mkdir build
  cd build
  cmake .. -DCMAKE_INSTALL_PREFIX=/path/to/install/dir/ -DFFTW_DIR=/path/to/fftw/
  make -j$(nproc)
  make install

Usage
-----

``psr-gen`` builds a ``psr_gen`` executable that will create DADA files containing simulated pulsar data:

.. code-block::

  psr_gen -t 0.1

This will create roughly 0.1 seconds of 40 MHz wide pulsar data, with a period
of 0.00575745 and a DM of 2.64476. We can see the default values for all the parameters by passing ``-h`` or ``--help``:

.. code-block::

  psr_gen --help

  Write simulated pulsar data to DADA file
  Usage:
    psr_gen [OPTION...]

        --file_name arg    Output filename (default: simulated_pulsar.dump)
    -v, --verbose          Verbose output
    -h, --help             Help
    -t, --time arg         time
    -n, --noise arg        noise parameter (default: 0.0)
    -D, --DM arg           dispersion measure (default: 2.64476)
    -c, --period arg       pulsar period (default: 0.00575745)
    -b, --bandwidth arg    bandwidth (default: 40)
    -f, --centre_freq arg  centre frequency (default: 1405.0)
    -l, --len_fft arg      Length of input FFT (default: 1048576)
    -p, --phase_bins arg   Pulsar phase bins (default: 1024)
        --gpu              Use GPU engine


``psr-gen`` also exposes an API allowing us to generate simulated pulsar data from within our own code:


.. code-block:: c++

  #include <vector>

  #include "psr_gen/PulsarGenerator.hpp"

  int main ()
  {
    psr_gen::PulsarGenerator<float> gen(
      5, // bandwidth
      1405, // centre_freq
      2, // npol
      0.1, // DM
      0.00575745, // period
      0.8, // noise
      32768, // len_fft
      1024 // phase_bins
    );

    std::vector<std::complex<float>> out;
    gen.compute(0.1, out); // compute one second worth of data.

    return 0;
  }



Let's say that we've installed ``psr-gen`` in ``$HOME/include``, and FFTW is installed in ``$FFTW_DIR``. We could build this as follows:

.. code-block::

  g++ -fopenmp -std=c++11 -I$HOME/include -I$HOME/linux_64/fftw-3.3.8/include
  -L$HOME/linux_64/fftw-3.3.8/lib64 -lfftw3 -lfftw3f  -o write_psr examples/write_psr.cpp


Benchmarking
------------

``psr-gen`` builds an executable called ``bench_PulsarGenerator`` which can be used in a similar manner to the main ``psr_gen`` exectuable. This times the amount of time it takes to generate a certain amount of data. These are by no means exhaustive benchmarks, in that they call ``psr_gen::PulsarGenerator::operate`` only once.

On a 2.30 GHz Intel Xeon using a single OpenMP thread:

.. code-block::

  OMP_NUM_THREADS=1 bench_PulsarGenerator -t 1.0
  Took 76252.8 ms to generate 1 s of data

On a 2.30 GHz Intel Xeon with four cores:

.. code-block::

  bench_PulsarGenerator -t 1.0
  Took 30872.5 ms to generate 1 s of data

On a Tesla P100:

.. code-block::

  bench_PulsarGenerator -t 1.0 --gpu
  Took 3206.41 ms to generate 1 s of data

This is a little difficult to benchmark against the original Matlab code, as the Matlab code incrementally saves data to disk. However, running Matlab in the ``matlab`` subdirectory:

.. code-block::

  >> write_simulated_pulsar
  Elapsed time is 95.355326 seconds.


.. <!-- cmake .. -DFFTW_DIR=$FRED/software/install/fftw-3.3.8
..
.. ```
.. gcovr -r . build -e test/toml.h -e test/catch.hpp -f include/*
.. ``` -->
